/// Dummy rules
struct DummyInjectionPreset : InjectionRulesPreset {

    var rules : [InjectionRule] = []
    
    init() {
        var compoundRules : [InjectionRule] = []
        compoundRules += self.managersRules
        compoundRules += self.valdiatorsRules
        self.rules = compoundRules
    }
    
    // MARK: - Manager Rules
    var managersRules : [InjectionRule] {
        return [InjectionRule(injectable: AuthorizationManager.self, injected: DummyAuthorizationManager()),
                InjectionRule(injectable: ProfileManager.self, injected: DummyProfileManager()),
                InjectionRule(injectable: ArticlesManager.self, injected: DummyArticlesManager()),
                InjectionRule(injectable: ProductsManager.self, injected: DummyProductsManager()),
                InjectionRule(injectable: RequestsManager.self, injected: DummyRequestsManager()),
                InjectionRule(injectable: StockManager.self, injected: DummyStockManager())
        ]
    }
    
}

// MARK: - Validation Rules
extension DummyInjectionPreset {
    
    private var loginValidators : [Validator] {
        return try! Injector.inject([Validator].self, with: ValidatorTypeInjectionParameter.LoginValidators)
    }
    
    private var passwordValidators : [Validator] {
        return try! Injector.inject([Validator].self, with: ValidatorTypeInjectionParameter.PasswordValidators)
    }
    
    var valdiatorsRules : [InjectionRule] {
        return [InjectionRule(injectable: [Validator].self, targetType: ValidatorTypeInjectionParameter.self) {
            switch $0 {
            case .LoginValidators: return [NilValidator(), LengthValidator(minLength: 4)]
            case .PasswordValidators: return [NilValidator(), LengthValidator(minLength: 4)]
            case .NameValidators: return [NilValidator(), LengthValidator(minLength: 4)]
            }
            }
        ]
    }
}