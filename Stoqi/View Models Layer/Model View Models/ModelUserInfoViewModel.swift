struct ModelUserInfoViewModel: UserInfoViewModel {
	
	var profile: Profile?{
		guard let name = name.value,
			email = email.value,
			phone = phone.value where isValid else {
			return nil
		}
		return Profile(name: name, email: email, phone: phone)
	}
	
	var name: ValidatableProperty<String>
	var email: ValidatableProperty<String>
	var phone: ValidatableProperty<String>
	
	init(profile: Profile){
		self.name = ValidatableProperty(value: profile.name, validators: [NilValidator()])
		self.email = ValidatableProperty(value: profile.email, validators: [EmailValidator()])
		self.phone = ValidatableProperty(value: profile.phone, validators: [NilValidator(), NumbersValidator(), LengthValidator(minLength:9)])
	}
	
}