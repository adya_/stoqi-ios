struct ModelAddressViewModel : AddressViewModel {
    var address : Address? {
        guard let street = street.value,
        building = building.value,
            zip = zip.value where isValid else {
                return nil
        }
        return Address(street: street, building: building, zip: zip)
    }
    
    var fullAddress : String? {
        guard let street = street.value,
            building = building.value,
            zip = zip.value where isValid else {
                return nil
        }
        return "\(building) \(street), \(zip)"
    }
    
    var street: ValidatableProperty<String>
    var building: ValidatableProperty<String>
    var zip: ValidatableProperty<String>
    
    init(address : Address? = nil) {
        street = ValidatableProperty(value: address?.street, validators: [NilValidator(), LengthValidator(minLength: 10)])
        building = ValidatableProperty(value: address?.building, validators: [NilValidator(), LengthValidator(minLength: 1)])
        zip = ValidatableProperty(value: address?.zip, validators: [NilValidator(), LengthValidator(exactLength: 5)])
    }
    
    var validatables : [Validatable] {
        return [street, building, zip]
    }
}