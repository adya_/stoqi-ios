struct ModelProductViewModel : ProductViewModel {
    
    var productEntry: ProductEntry? {
        guard let kind = self.kind?.kind,
            brand = self.brand?.brand,
            volume = self.volume?.volume else {
                return nil
        }
        let product = Product(id: nil, category: self.category, kind: kind, brand: brand, volume: volume, price: 0)
        return ProductEntry(product: product, units: self.units)
    }

    
    var kind: KindPickerItemViewModel? = nil
    var brand : BrandPickerItemViewModel? = nil
    var volume: VolumePickerItemViewModel? = nil
    var units: Int = 0
   
    private let category : Category
    
    init(category : Category) {
        self.category = category
    }
    
    init(product : ProductEntry) {
        self.brand = ModelBrandPickerItemViewModel(brand: product.product.brand)
        self.volume = ModelVolumePickerItemViewModel(volume: product.product.volume)
        self.kind = ModelKindPickerItemViewModel(kind: product.product.kind)
        self.category = product.product.category
        self.units = product.units
    }
    
    var isValid: Bool {
        return self.kind != nil &&
            self.brand != nil &&
            self.volume != nil &&
            self.units > 0
    }
}