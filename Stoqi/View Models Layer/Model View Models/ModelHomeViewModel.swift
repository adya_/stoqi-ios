struct ModelHomeViewModel : HomeViewModel {
    var minDeliveryDate: NSDate
    var maxDeliveryDate: NSDate
    var userName: String
    var totalSaved: Float
    var monthlySavings: Float
	var canRefill : Bool
    
	init?(profile : Profile, requests: [RequestList]) {
        self.userName = profile.name ?? profile.email ?? "Guest"
        self.totalSaved = profile.saved
        self.monthlySavings = profile.monthly
		
		let newRequest = requests.filter {
			if case .New = $0.status{ return true }
			return false
		}.first
		
		let pendingRequest = requests.filter {
			if case .Pending = $0.status{ return true }
			return false
			}.first
		
		let request = newRequest == nil ? pendingRequest! : newRequest!
		
		do {
			self.minDeliveryDate = try {
				if case .New(let from, _) = request.status{
					return from
				} else if case .Pending(let from, _) = request.status{
					return from
				}
				throw Error.MinDate
				}()
			
			self.maxDeliveryDate = try {
				if case .New(_, let to) = request.status{
					return to
				} else if case .Pending(_, let to) = request.status{
					return to
				}
				throw Error.MaxDate
			}()
		}
		catch
		{
			return nil
		}
		
		self.canRefill = false
		self.canRefill = {
			return self.minDeliveryDate.timeIntervalSince1970 - NSDate().timeIntervalSince1970 <= 60*60*24*7*2
		}()
    }
}

private enum Error: ErrorType {
	case MinDate
	case MaxDate
}