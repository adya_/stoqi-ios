struct ModelCardViewModel : CardViewModel {
    var card : Card? {
        guard let number = number.value,
        owner = owner.value,
        code = code.value,
        month = month.value,
            year = year.value where isValid else {
                return nil
        }
        return Card(id: nil, number: number, code: code, owner: owner, month: month, year: year)
    }
    var cardTypeImage: UIImage?
    var number : ValidatableProperty<String>
    var owner : ValidatableProperty<String>
    var code : ValidatableProperty<String>
    var month : ValidatableProperty<Int>
    var year : ValidatableProperty<Int>
    
    init(card : Card? = nil) {
        number = ValidatableProperty(value: card?.number, validators: [NilValidator(), VisaValidator() | AMEXValidator() | MasterCardValidator()])
        owner = ValidatableProperty(value: card?.owner, validators: [NilValidator()])
        code = ValidatableProperty(value: card?.code, validators: [NilValidator(), LengthValidator(minLength: 3, maxLength: 4)])
        month = ValidatableProperty(value: card?.month, validators: [NilValidator(), ValueValidator(minValue: 1, maxValue: 12)])
        year = ValidatableProperty(value: card?.year, validators: [NilValidator(), ValueValidator(minValue: NSDate().year)])
//        self.cardTypeImage = UIImage(named: "visa")
        // TODO: Complete card view model
    }
    
    var validatables : [Validatable] {
        return [number, owner, code, month, year]
    }

}