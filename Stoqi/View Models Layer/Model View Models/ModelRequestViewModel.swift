struct ModelRequestViewModel : RequestViewModel {
    let request: RequestList
    var date: NSDate {
        if case .Completed(let date) = self.request.status {
            return date
        } else {
            return NSDate()
        }
    }
    var startDate: NSDate? {
        if case .Pending(let from, _) = self.request.status {
            return from
        } else if case .New = self.request.status {
            return nil
        } else {
            return NSDate()
        }
    }

    var endDate: NSDate? {
        if case .Pending(_, let to) = self.request.status {
            return to
        } else if case .New = self.request.status {
            return nil
        } else {
            return NSDate()
        }
    }
    
    var hasNextNode: Bool
    
    var price: Float {
        return self.request.total
    }
    var items: Int {
        return self.request.count
    }
    
    init(request : RequestList, hasNextNode : Bool = true) {
        self.request = request
        self.hasNextNode = hasNextNode
    }
}