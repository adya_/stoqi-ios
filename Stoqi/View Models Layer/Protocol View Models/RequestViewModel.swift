protocol RequestViewModel : HistoryRequestCellDataSource, CurrentRequestCellDataSource {
    var request : RequestList {get}
    var date: NSDate  {get}
    var startDate: NSDate? {get}
    var endDate: NSDate? {get}
    var hasNextNode: Bool {get set}
    var price: Float {get}
    var items: Int { get}
}