protocol StockViewModel {
    var stock : Stock? {get}
    var date : NSDate {get}
    
    var categories: [StockProductsCategoryViewModel] {get set}
}