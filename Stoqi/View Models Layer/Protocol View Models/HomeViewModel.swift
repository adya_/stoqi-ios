protocol HomeViewModel {
    var minDeliveryDate : NSDate {get}
    var maxDeliveryDate : NSDate {get}
    var userName : String {get}
    var totalSaved : Float {get}
    var monthlySavings : Float {get}
	var canRefill : Bool {get}
}