typealias RegisterListPageInjectionParameter = (question : QuestionViewModel,
    pageIndex : Int,
    totalPages : Int,
    canNavigateNext : Bool)

typealias SearchPropertyLocationInjectionParameter = (location : PropertyLocation, matchingTerm : String?)

typealias ProductsCategoryInjectionParameter = (category : Category, products : [ProductEntry])

typealias StockProductsCategoryInjectionParameter = (category : Category, products : [StockProduct])

typealias HomeInjectionParameter = (profile : Profile, requests : [RequestList])

typealias StringPickerDataSourceInjectionParameter = (title : String, values : [String], initialValue : String?)

enum ValidatorTypeInjectionParameter {
    case LoginValidators
    case PasswordValidators
    case NameValidators
}

enum PickerInjectionParameter {
    case MonthsPicker
    case YearsPicker
}