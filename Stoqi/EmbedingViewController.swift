class EmbedingViewController : BaseViewController {
    @IBOutlet weak private var vContent : UIView!
    
    func embed<T : StandaloneViewController>(controllerType : T.Type) -> T {
        let controller = controllerType.init()
        embed(controller)
        return controller
    }
    
    func embed(controller : UIViewController) -> UIViewController {
        addChildViewController(controller)
        vContent.addSubview(controller.view)
        controller.view.translatesAutoresizingMaskIntoConstraints = false
        let constraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[content]|", options: [], metrics: nil, views: ["content" : controller.view]) +
            NSLayoutConstraint.constraintsWithVisualFormat("V:|[content]|", options: [], metrics: nil, views: ["content" : controller.view])
        NSLayoutConstraint.activateConstraints(constraints)
        controller.didMoveToParentViewController(self)
        return controller
    }
}