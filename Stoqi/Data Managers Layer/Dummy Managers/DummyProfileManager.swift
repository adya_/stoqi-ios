import GCDKit

private let kStorageCacheProfile = "kStorageDummyCacheProfile"

class DummyProfileManager : ProfileManager {
    
    var profile: Profile?
    
    init() {
        restoreCachedProfileRegistration()
    }
    
    func performSearchPropertyLocation(withTerm term: String, callback: PropertyLocationCallback) {
        GCDQueue.Main.after(1) {
            let searchableTerm = term.lowercaseString
            callback(.Success(self.dynamicType.createDummyLocations().filter({
                $0.cityName.lowercaseString.containsString(searchableTerm) ||
                    $0.regionName.lowercaseString.containsString(searchableTerm)
            })))
        }
    }
    
    func performCreateProfile(profile: Profile, callback: OperationCallback) {
        self.profile = profile
        GCDQueue.Main.after(1) {
            callback(.Success)
        }
    }
    
    func performSaveProfile(profile: Profile, callback: OperationCallback) {
        self.profile = profile
        GCDQueue.Main.after(1) {
            callback(.Success)
        }
    }
    
    func performLoadProfile(callback: ProfileCallback) {
        self.profile = self.dynamicType.createDummyProfile()
        GCDQueue.Main.after(1) { 
            callback(.Success(self.profile!))
        }
    }
    
    func checkProfileRegistered() -> Bool {
        guard let profile = profile else {
            return false
        }
        return profile.name != nil &&
            profile.location != nil &&
            profile.propertyType != nil &&
            profile.propertyArea != nil &&
            profile.propertyResidents != nil &&
            profile.priority != nil
    }
    
    func cacheProfileRegistration(profile : Profile) {
        let accountManager = try! Injector.inject(AuthorizationManager.self)
        let account = accountManager.account?.email ?? accountManager.account?.facebookToken
        guard let key = account else {
            return
        }
        var cache = Storage.local[kStorageCacheProfile] as? [String : AnyObject] ?? [:]
        cache[key] = profile.archived()
        Storage.local[kStorageCacheProfile] = cache
    }
    
    func restoreCachedProfileRegistration() {
        let accountManager = try! Injector.inject(AuthorizationManager.self)
        let account = accountManager.account?.email ?? accountManager.account?.facebookToken
        guard let key = account else {
            return
        }
        profile = ((Storage.local[kStorageCacheProfile] as? [String : AnyObject])?[key] as? [String : AnyObject]).flatMap{Profile(fromArchive: $0)}
    }
    
    func clearCache() {
        guard var cache = Storage.local[kStorageCacheProfile] as? [String : AnyObject] else {
            return
        }
        let accountManager = try! Injector.inject(AuthorizationManager.self)
        let account = accountManager.account?.email ?? accountManager.account?.facebookToken
        guard let key = account else {
            return
        }
        cache[key] = nil
        Storage.local[kStorageCacheProfile] = cache
    }
}

extension DummyProfileManager {

    class func createDummyLocations() -> [PropertyLocation] {
        return [PropertyLocation(id: 1, cityName: "Sao Paolo", regionName: "SP"),
                PropertyLocation(id: 2, cityName: "Sao Jose dp Rio Preto", regionName: "SP"),
                PropertyLocation(id: 3, cityName: "Sao Jose do Xingu", regionName: "MT"),
                PropertyLocation(id: 4, cityName: "Sao Bernardo do Campo", regionName: "SP"),
                PropertyLocation(id: 5, cityName: "Test City 1", regionName: "TR1"),
                PropertyLocation(id: 6, cityName: "Test City 2", regionName: "TR1"),
                PropertyLocation(id: 7, cityName: "Test City 3", regionName: "TR1"),
                PropertyLocation(id: 8, cityName: "Test City 4", regionName: "TR1"),
                PropertyLocation(id: 9, cityName: "Test City 1", regionName: "TR2"),
                PropertyLocation(id: 10, cityName: "Test City 2", regionName: "TR2"),
                PropertyLocation(id: 11, cityName: "Test City 1", regionName: "TR3")
        ]
    }
    
    class func createDummyProfile() -> Profile {
        let locations = self.createDummyLocations()
        let area = PropertyArea(area: 60, rooms: 2, knownArea: true)
        let residents = PropertyResidents(adults: 2, children: 1, pets: 1)
        return Profile(email: "tester@test.test",
                       name: "Tester",
                       saved: 150,
                       monthly: 30,
                       location: locations[0],
                       propertyType: .Apartment,
                       propertyArea: area,
                       propertyResidents: residents,
                       priority: .Both)
    }
}