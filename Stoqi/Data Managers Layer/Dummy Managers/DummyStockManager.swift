import GCDKit

class DummyStockManager : DummyAuthorizedManager, StockManager {
    var stock: Stock?
    
    func performLoadStock(callback: LoadStockCallback) {
        self.stock = self.dynamicType.dummy
        GCDQueue.Main.after(1) {
            callback(.Success(self.stock!))
        }
    }
    
    func performSaveStock(stock: Stock, callback: OperationCallback) {
        self.stock = stock
        GCDQueue.Main.after(1) {
            callback(.Success)
        }
    }
}

extension DummyStockManager {
    class var dummy : Stock {
        let products : [StockProduct] = DummyProductsManager.dummies.random(3).map {
            let total = (1...10).random
            let left = (0...total).random
            return StockProduct(product: $0, left: Float(left)  * 0.25, total: Float(total) * 0.25)
        }
        return Stock(products: products)
    }
}