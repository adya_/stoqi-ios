import GCDKit

class DummyRequestsManager : DummyAuthorizedManager, RequestsManager {
    var requests: [RequestList]?
    
    func performLoadRequests(callback: LoadRequestsCallback) {
        self.requests = self.dynamicType.dummies
        GCDQueue.Main.after(1) {
            callback(.Success(self.requests!))
        }
    }
    
    func performLoadRequestList(requestList: RequestList, callback: LoadRequestListCallback) {
        GCDQueue.Main.after(1) {
            callback(.Success(requestList))
        }
    }
    
    func performProcessRequest(request: RequestList, callback: OperationCallback) {
        guard case .Pending = request.status else {
            callback(.Failure(.InvalidParameters))
            return
        }
        var request = request
        request.status = .Completed(date: NSDate())
        self.requests?[request] = request
        let id = (self.requests?.last?.id ?? self.dynamicType.dummies.last?.id ?? 0) + 1
        let newRequest = RequestList(id: id,
                                 products: self.dynamicType.requestedProducts(DummyProductsManager.dummies.random(4)),
                                 status: .Pending(from: NSDate(), to: (NSDate() + TSDateComponents.Months(1))!))
        self.requests?.append(newRequest)
        GCDQueue.Main.after(1) {
            callback(.Success)
        }
    }
    
    func performSaveRequest(request: RequestList, callback: OperationCallback) {
        self.requests?[request] = request
        GCDQueue.Main.after(1) {
            callback(.Success)
        }
    }
}

extension DummyRequestsManager {
    
    private class func requestedProducts(products : [Product]) -> [ProductEntry] {
        return products.map {ProductEntry(product: $0, units: (1...5).random)}
    }
    
    class var dummies : [RequestList] {
        let products = DummyProductsManager.dummies
        return [
            RequestList(id: 1,
                products: self.requestedProducts(products.random(3)),
                status: .Completed(date: NSDate.fromString("24/06/2016", withFormat: "dd/MM/yyyy")!)),
            RequestList(id: 2,
                products: self.requestedProducts(products.random(3)),
                status: .Completed(date: NSDate.fromString("23/04/2016", withFormat: "dd/MM/yyyy")!)),
            RequestList(id: 3,
                products: self.requestedProducts(products.random(2)),
                status: .Completed(date: NSDate.fromString("24/02/2016", withFormat: "dd/MM/yyyy")!)),
            RequestList(id: 5,
				products: self.requestedProducts(products.random(2)),
				status: .New(from: NSDate.fromString("28/08/2017", withFormat: "dd/MM/yyyy")!,
								 to: NSDate.fromString("04/09/2017", withFormat: "dd/MM/yyyy")!)),
            RequestList(id: 4,
                products: self.requestedProducts(products.random(2)),
                status: .Pending(from: NSDate.fromString("28/08/2016", withFormat: "dd/MM/yyyy")!,
                                   to: NSDate.fromString("04/09/2016", withFormat: "dd/MM/yyyy")!))
            
        ]
    }
}
