class DummyAuthorizedManager {
    init() {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(authorize), name: AuthorizationManagerAuthorizedNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(deauthorize), name: AuthorizationManagerDeauthorizedNotification, object: nil)
        
    }
    
    @objc func deauthorize(notification : NSNotification) {
        print("\(self.dynamicType): Deauthorized.")
    }
    
    @objc func authorize(notification : NSNotification) {
        print("\(self.dynamicType): Authorized.")
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
}