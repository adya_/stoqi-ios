import GCDKit

private let kAccount = "authorizedAccountDummy"

class DummyAuthorizationManager : AuthorizationManager {
    
    private static let dummyEmail = "test@test.test"
    
    var account: Account? {
        didSet {
            if account != nil {
                authorize()
                saveAuthorizedUser()
            } else {
                deauthorize()
                clearAuthorizedUser()
            }
        }
    }

    
    func performAuthorization(withCredentials credentials: Credentials, callback : AccountCallback) {
        let fakeRegister = credentials.email == self.dynamicType.dummyEmail
        if !fakeRegister {
            self.account = self.dynamicType.createDummyAccount(credentials)
        }
        GCDQueue.Main.after(1)  {
            callback(fakeRegister ? .NotExisted : .Authorized(self.account!))
        }
    }
    
    func performFacebookAuthorization(token : String, callback: AccountCallback) {
        self.account = self.dynamicType.createDummyAccount()
        GCDQueue.Main.after(1)  {
            callback(.Created(self.account!))
        }
    }
	
	func performPasswordRecovery(email: String, callback: OperationCallback?) {
		callback?(.Success)
	}
    
    func performLogout(callback : OperationCallback? = nil) {
        self.account = nil
        callback?(.Success)
    }
    
    func performRegistration(withCredentials credentials: Credentials, callback : AccountCallback) {
        self.account = self.dynamicType.createDummyAccount(credentials)
        GCDQueue.Main.after(1)  {
            callback(.Created(self.account!))
        }
    }
    
    class func createDummyAccount(base : Credentials? = nil) -> Account {
        let email = base?.email ?? self.dummyEmail
        return Account(id: 1, email: email)
    }
}

// MARK: - Sinlge Sign-In
extension DummyAuthorizationManager {
    func saveAuthorizedUser() {
        Storage.local[kAccount] = account?.archived()
    }
    
    func restoreAuthorizedUser() {
        account = (Storage.local[kAccount] as? [String : AnyObject]).flatMap{Account(fromArchive: $0)}
        authorize()
    }
    
    func clearAuthorizedUser() {
        Storage.local[kAccount] = nil
    }
}

// MARK: - Authorization Notifications
extension DummyAuthorizationManager {
    func authorize() {
        NSNotificationCenter.defaultCenter().postNotificationName(AuthorizationManagerAuthorizedNotification, object: nil)
    }
    
    func deauthorize() {
        NSNotificationCenter.defaultCenter().postNotificationName(AuthorizationManagerDeauthorizedNotification, object: nil)
    }
}
