import FBSDKLoginKit
import FBSDKCoreKit

enum FacebookResult {
    case Authorized(token : String)
    case Canceled
    case Failed
}

class FacebookHelper {
    class func authorizeFacebook(fromViewController controller: UIViewController, callback : ( FacebookResult) -> Void) {
        let fb = FBSDKLoginManager()
        fb.logInWithReadPermissions(["email", "public_profile"], fromViewController: controller) { (result, error) in
            guard let result = result else {
                callback(.Failed)
                return
            }
            
            guard !result.isCancelled else {
                callback(.Canceled)
                return
            }
            
            let token = result.token.tokenString
            callback(.Authorized(token: token))
        }
    }
    
    class func getUser(callback : (OperationResult<(name : String, email : String)>) -> Void) {
        guard let token = FBSDKAccessToken.currentAccessToken() else {
            callback(.Failure(.NotAuthorized))
            return
        }
        
        FBSDKGraphRequest(graphPath: "me", parameters: ["fields" : "name, email"], tokenString: token.tokenString, version: nil, HTTPMethod: "GET").startWithCompletionHandler { (_, res, _) in
            guard let name = res?["name"] as? String,
            email = res?["email"] as? String else {
                callback(.Failure(.InvalidResponse))
                return
            }
            callback(.Success(name: name, email: email))
        }
    }
}