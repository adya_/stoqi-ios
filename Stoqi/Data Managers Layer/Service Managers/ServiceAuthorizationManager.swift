private let kAccount = "authorizedAccount"

class ServiceAuthorizationManager : ServiceManager, AuthorizationManager {
    var account: Account? {
        didSet {
            if account != nil {
                authorize()
                saveAuthorizedUser()
            } else {
                deauthorize()
                clearAuthorizedUser()
            }
        }
    }
    
    override init(requestManager : RequestManager) {
        super.init(requestManager: requestManager)
        self.restoreAuthorizedUser()
    }
    
    func performAuthorization(withCredentials credentials: Credentials, callback: AccountCallback) {
        let request = LoginRequest(credentials: credentials)
        let call = RequestCall(request: request, responseType: AccountResponse.self) {
            switch $0 {
            case .Success(let response):
                if case .ExistingAccount(let account) = response.value {
                    self.account = Account(id: account.id, email: credentials.email)
                    callback(.Authorized(account))
                } else {
                    callback(.NotExisted)
                }

            case .Failure(let error):
                callback(.Failure(OperationError(error: error)))
            }
        }
        self.requestManager.request(call)
    }
    
    func performRegistration(withCredentials credentials: Credentials, callback: AccountCallback) {
        let request = RegisterRequest(credentials: credentials)
        let call = RequestCall(request: request, responseType: AccountResponse.self) {
            switch $0 {
            case .Success(let response):
                if case .CreatedAccount(let account) = response.value {
                    self.account = Account(id: account.id, email: credentials.email)
                    callback(.Created(account))
                } else {
                    callback(.NotExisted)
                }
               
			case .Failure(let error):
				callback(.Failure(OperationError(error: error)))
			}
        }
        self.requestManager.request(call)
    }
	
	func performPasswordRecovery(email: String, callback: OperationCallback?) {
		callback?(.Failure(OperationError.Unknown))
	}
    
    func performFacebookAuthorization(token : String, callback: AccountCallback) {
        let request = FacebookLoginRequest(token: token)
        let call = RequestCall(request: request, responseType: AccountResponse.self) {
            switch $0 {
            case .Success(let response):
                if case .ExistingAccount(let account) = response.value {
                    self.account = Account(id: account.id, facebookToken: token)
                    callback(.Authorized(account))
                } else if case .CreatedAccount(let account) = response.value {
                    self.account = Account(id: account.id, facebookToken: token)
                    callback(.Created(account))
                } else {
                    callback(.NotExisted)
                }
			case .Failure(let error):
				callback(.Failure(OperationError(error: error)))
			}
        }
        self.requestManager.request(call)
    }
    
    func performLogout(callback: OperationCallback?) {
        self.account = nil
        deauthorize()
        clearAuthorizedUser()
    }
}

// MARK: - Authorization Notifications
extension ServiceAuthorizationManager {
    func authorize() {
        NSNotificationCenter.defaultCenter().postNotificationName(AuthorizationManagerAuthorizedNotification, object: nil)
    }
    
    func deauthorize() {
        NSNotificationCenter.defaultCenter().postNotificationName(AuthorizationManagerDeauthorizedNotification, object: nil)
    }
}

// MARK: - Sinlge Sign-In
extension ServiceAuthorizationManager {
    func saveAuthorizedUser() {
        Storage.local[kAccount] = account?.archived()
    }
    
    func restoreAuthorizedUser() {
        account = (Storage.local[kAccount] as? [String : AnyObject]).flatMap{Account(fromArchive: $0)}
        authorize()
    }
    
    func clearAuthorizedUser() {
        Storage.local[kAccount] = nil
    }
}