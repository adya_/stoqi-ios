private let kStorageCacheProfile = "kStorageCacheProfile"

class ServiceProfileManager : AuthorizedManager, ProfileManager {
    var profile : Profile?
    
    
    override init(requestManager : RequestManager, accountManager : AuthorizationManager) {
        super.init(requestManager: requestManager, accountManager: accountManager)
        restoreCachedProfileRegistration()
    }
    
    func performSearchPropertyLocation(withTerm term: String, callback: PropertyLocationCallback) {
        let call = RequestCall(request: SearchLocationRequest(term: term), responseType: PropertyLocationResponse.self) {
            switch $0 {
            case .Success(let response):
                callback(.Success(response.value))
			case .Failure(let error):
				callback(.Failure(OperationError(error: error)))
			}
        }
		
        requestManager.request(call)
    }
    
    func performCreateProfile(profile: Profile, callback: OperationCallback) {
        guard let account = accountManager.account,
            request = SaveProfileRequest(account: account, profile: profile) else {
                callback(.Failure(.InvalidParameters))
                return
        }
        
        let call = RequestCall(request: request, responseType: EmptyResponse.self) {
            switch $0 {
            case .Success:
                self.profile = self.profile?.intercect(profile) ?? profile
                callback(.Success)
			case .Failure(let error):
				callback(.Failure(OperationError(error: error)))
			}
        }
        requestManager.request(call)
    }

    
    func performSaveProfile(profile: Profile, callback: OperationCallback) {
        guard let account = accountManager.account,
            request = SaveProfileRequest(account: account, profile: self.profile?.diff(profile) ?? profile) else {
                callback(.Failure(.InvalidParameters))
                return
        }
        
        let call = RequestCall(request: request, responseType: EmptyResponse.self) {
            switch $0 {
            case .Success:
                self.profile = self.profile?.intercect(profile) ?? profile
                callback(.Success)
			case .Failure(let error):
				callback(.Failure(OperationError(error: error)))
			}
        }
        requestManager.request(call)
    }
    
    func performLoadProfile(callback: ProfileCallback) {
        guard let account = accountManager.account,
            request = LoadProfileRequest(account: account) else {
                callback(.Failure(.InvalidParameters))
                return
        }
        let call = RequestCall(request: request, responseType: ProfileResponse.self) {
            switch $0 {
            case .Success(let response):
                self.profile = response.value
                callback(.Success(self.profile!))
			case .Failure(let error):
				callback(.Failure(OperationError(error: error)))
			}
        }
        requestManager.request(call)
    }
    
    func checkProfileRegistered() -> Bool {
        guard let profile = profile else {
            return false
        }
        return profile.name != nil &&
            profile.location != nil &&
            profile.propertyType != nil &&
            profile.propertyArea != nil &&
            profile.propertyResidents != nil &&
            profile.priority != nil
    }
    
    override func deauthorize(notification: NSNotification) {
        super.deauthorize(notification)
        profile = nil
    }
}

extension ServiceProfileManager {
    func cacheProfileRegistration(profile : Profile) {
        let account = accountManager.account?.email ?? accountManager.account?.facebookToken
        guard let key = account else {
            return
        }
        var cache = Storage.local[kStorageCacheProfile] as? [String : AnyObject] ?? [:]
        cache[key] = profile.archived()
        Storage.local[kStorageCacheProfile] = cache
    }
    
    func restoreCachedProfileRegistration() {
        let account = accountManager.account?.email ?? accountManager.account?.facebookToken
        guard let key = account else {
            return
        }
        profile = ((Storage.local[kStorageCacheProfile] as? [String : AnyObject])?[key] as? [String : AnyObject]).flatMap{Profile(fromArchive: $0)}
    }
    
    func clearCache() {
        guard var cache = Storage.local[kStorageCacheProfile] as? [String : AnyObject] else {
            return
        }
        let account = accountManager.account?.email ?? accountManager.account?.facebookToken
        guard let key = account else {
            return
        }
        cache[key] = nil
        Storage.local[kStorageCacheProfile] = cache
    }
}