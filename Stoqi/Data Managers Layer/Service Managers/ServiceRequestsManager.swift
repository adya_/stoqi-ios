class ServiceRequestsManager : AuthorizedManager, RequestsManager {
    var requests: [RequestList]?
    
    func performLoadRequests(callback: LoadRequestsCallback) {
        guard let account = self.accountManager.account,
            request = LoadRequestsRequest(account: account) else {
                callback(.Failure(.NotAuthorized))
                return
        }
        
        let call = RequestCall(request: request, responseType: RequestsResponse.self){
            switch $0 {
            case .Success(let response):
                self.requests = response.value
                callback(.Success(response.value))
			case .Failure(let error):
				callback(.Failure(OperationError(error: error)))
			}
        }
        self.requestManager.request(call)
    }
    
    func performLoadRequestList(requestList: RequestList, callback: LoadRequestListCallback) {
        guard let request = LoadRequestProductsListRequest(requestList: requestList) else {
            callback(.Failure(.InvalidParameters))
            return
        }
        
        let call = RequestCall(request: request, responseType: RequestedProductsResponse.self) {
            switch $0 {
            case .Success(let response):
                var list = requestList
                list.products = response.value
                self.requests?[requestList] = requestList
                callback(.Success(list))
			case .Failure(let error):
				callback(.Failure(OperationError(error: error)))
			}
        }
        self.requestManager.request(call)
    }
    
    func performSaveRequest(requestList: RequestList, callback: OperationCallback) {
        guard let account = self.accountManager.account,
            request = SaveRequestListRequest(account: account, requestList: requestList) else {
                callback(.Failure(.NotAuthorized))
                return
        }
        let call = RequestCall(request: request, responseType: EmptyResponse.self){
            switch $0 {
            case .Success:
                callback(.Success)
			case .Failure(let error):
				callback(.Failure(OperationError(error: error)))
			}
        }
        self.requestManager.request(call)
    }
    
    func performProcessRequest(requestList: RequestList, callback: OperationCallback) {
        guard let request = ProcessRequestListRequest(requestList: requestList) else {
                callback(.Failure(.NotAuthorized))
                return
        }
        let call = RequestCall(request: request, responseType: RequestListResponse.self){
            switch $0 {
            case .Success(let response):
                self.requests?.append(response.value)
                callback(.Success)
			case .Failure(let error):
				callback(.Failure(OperationError(error: error)))
			}
        }
        self.requestManager.request(call)
    }
    
    override func deauthorize(notification: NSNotification) {
        super.deauthorize(notification)
        self.requests = nil
    }
}