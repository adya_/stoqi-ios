
class ServiceStockManager : AuthorizedManager, StockManager {
    var stock: Stock?
    
    // TODO: Requests
    func performLoadStock(callback: LoadStockCallback) {
        guard let account = accountManager.account,
            request = LoadStockRequest(account: account) else {
            callback(.Failure(.NotAuthorized))
            return
        }
        let call = RequestCall(request: request, responseType: StockResponse.self) {
            if case .Success(let response) = $0 {
                self.stock = response.value
                callback(.Success(response.value))
			} else if case .Failure(let error) = $0 {
				callback(.Failure(OperationError(error: error)))
			}
        }
        requestManager.request(call)
    }
    
    func performSaveStock(stock: Stock, callback: OperationCallback) {
        guard let account = accountManager.account,
            request = SaveStockRequest(account: account, stock: stock) else {
                callback(.Failure(.NotAuthorized))
                return
        }
        let call = RequestCall(request: request, responseType: EmptyResponse.self) {
            if case .Success = $0 {
                self.stock = stock
                callback(.Success)
			} else if case .Failure(let error) = $0 {
				callback(.Failure(OperationError(error: error)))
			}
        }
        requestManager.request(call)
    }
}