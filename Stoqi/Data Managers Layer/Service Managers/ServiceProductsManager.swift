class ServiceProductsManager : ServiceManager, ProductsManager {
    var products: [Product]?
    var categories: [Category]?
    
    func performLoadProducts(callback: LoadProductsCallback) {
        let call = RequestCall(request: LoadProductsRequest(), responseType: CategoriesResponse.self) {
            switch $0 {
            case .Success(let response):
                self.products = Array(response.value.values.flatMap{$0})
                self.categories = Array(response.value.keys)
                callback(.Success(self.products!))
			case .Failure(let error):
				callback(.Failure(OperationError(error: error)))
			}
        }
		
        self.requestManager.request(call)
    }
    
    func performFindProduct(barcode: String, callback: FindProductCallback) {
        let call = RequestCall(request: FindProductRequest(barcode: barcode), responseType: ProductResponse.self) {
            switch $0 {
            case .Success(let response):
                callback(.Success(response.value))
			case .Failure(let error):
				callback(.Failure(OperationError(error: error)))
			}
        }
		
        self.requestManager.request(call)
    }
}