

/// Occurs when AuthorizationManager has obtained token. Use this notification to setup relying managers.
let AuthorizationManagerAuthorizedNotification = "AuthorizationManagerAuthorizedNotification"

/// Occurs when AuthorizationManager has cleared token. Use this notification to clear all data related to that token.
let AuthorizationManagerDeauthorizedNotification = "AuthorizationManagerDeauthorizedNotification"

enum AuthorizationResult {
    case Authorized(Account)
    case Created(Account)
    case NotExisted
    case Failure(OperationError)
}

typealias AccountCallback = (AuthorizationResult) -> Void

/// Used for authorization process.
protocol AuthorizationManager {
    
    var account : Account? {get}
     
    /**
     - Throws:
     * OperationError.InvalidParameters,
     * OperationError.InvalidResponse,
     * OperationError.NetworkError,
     * OperationError.NotAuthorized
     */
    func performAuthorization(withCredentials credentials: Credentials, callback : AccountCallback)
    
    func performRegistration(withCredentials credentials: Credentials, callback : AccountCallback)
    
    /**
     - Throws:
     * OperationError.InvalidResponse,
     * OperationError.NetworkError
     */
    func performFacebookAuthorization(facebookToken: String, callback : AccountCallback)
	
	func performPasswordRecovery(email: String, callback : OperationCallback?)
    
    /// Commonly simply clears account data.
    /// - Note: Conformed types must send notifications described above to let other managers to clear their account related data.
    func performLogout(callback : OperationCallback?)
}

extension AuthorizationManager {
    func performLogout() {
        self.performLogout(nil)
    }
}