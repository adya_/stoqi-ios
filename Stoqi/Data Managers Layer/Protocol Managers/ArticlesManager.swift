typealias ArticlesCallback = (OperationResult<[Article]>) -> Void

protocol ArticlesManager {
    
    var articles : [Article]? {get}
    
    func performLoadArticles(callback : ArticlesCallback)
}