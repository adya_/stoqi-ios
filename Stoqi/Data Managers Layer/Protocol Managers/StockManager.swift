typealias LoadStockCallback = (OperationResult<Stock>) -> Void

protocol StockManager {
    var stock : Stock? {get}
    
    func performLoadStock(callback : LoadStockCallback)
    func performSaveStock(stock : Stock, callback : OperationCallback)
}