typealias LoadProductsCallback = (OperationResult<[Product]>) -> Void
typealias FindProductCallback = (OperationResult<Product>) -> Void
/// Contains product definitions
protocol ProductsManager {
    
    /// List of products available in the Stoqi
    var products : [Product]? {get}
    
    var categories : [Category]? {get}
    
    // TODO: Pagination is mandatory here
    
    /// Loads list of available products
    func performLoadProducts(callback : LoadProductsCallback)
    
    func performFindProduct(barcode : String, callback : FindProductCallback)
}