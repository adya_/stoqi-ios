typealias PropertyLocationCallback = (OperationResult<[PropertyLocation]>) -> Void
typealias ProfileCallback = (OperationResult<Profile>) -> Void

protocol ProfileManager {
    
    var profile : Profile? {get}
    
    func performSearchPropertyLocation(withTerm term : String, callback : PropertyLocationCallback)
    
    func performCreateProfile(profile : Profile, callback : OperationCallback)
    
    func performSaveProfile(profile : Profile, callback : OperationCallback)
    
    func performLoadProfile(callback : ProfileCallback)
    
    func checkProfileRegistered() -> Bool
    
    func cacheProfileRegistration(profile : Profile)
    
    func clearCache()
}