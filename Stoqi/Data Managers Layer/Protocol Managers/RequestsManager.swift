typealias LoadRequestsCallback = (OperationResult<[RequestList]>) -> Void
typealias LoadRequestListCallback = (OperationResult<RequestList>) -> Void

/// Manages user's requests
protocol RequestsManager {
    
    /// List of user's requests.
    var requests : [RequestList]? {get}
    
    /// Loads user's both history and pending requests.
    func performLoadRequests(callback : LoadRequestsCallback)
    
    /// Loads products list of given request.
    func performLoadRequestList(requestList : RequestList, callback : LoadRequestListCallback)
    
    /// Initiates request processing.
    func performProcessRequest(request : RequestList, callback : OperationCallback)
    
    /// Saves changes in the request
    func performSaveRequest(request : RequestList, callback : OperationCallback)
}