import UIKit
import ActionSheetPicker_3_0

enum InputErrors : ErrorType {
    case MissingFields([String]?)
    case InvalidFields([String]?)
}

class BaseViewController : TSViewControllerNavEx {
    
    /// Indicates that view is loaded and can be configured with ViewModel
    private(set) var isLoaded : Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        self.isLoaded = true
    }
    
    override func viewWillAppear(animated: Bool) {
        TSNotifier.hideProgress()
        super.viewWillAppear(animated)
    }
    
    func openPicker(dataSource : PickerDataSource, trigger : Triggerable, selectedBlock : (Int, PickerItemViewModel) -> Void) {
        var index : Int = 0
        if let value = dataSource.initialValue, matchIndex = dataSource.values.indexOf({value.itemText == $0.itemText}) {
            index = matchIndex
        }
        trigger.isTriggered = true
        ActionSheetStringPicker(title: dataSource.pickerTitle, rows: dataSource.values.map {$0.itemText}, initialSelection: index, doneBlock: { (_, selected, _) in
            selectedBlock(selected, dataSource.values[selected])
            trigger.isTriggered = false
            }, cancelBlock: { (_) in
                trigger.isTriggered = false
            }, origin: trigger).showActionSheetPicker()
    }
    
    /// Sets color of target view's border for either valid or invalid state.
    func setView(view : UIView, valid : Bool, validColor : UIColor = StoqiPallete.auxColor, invalidColor : UIColor = UIColor.redColor()) {
        view.borderColor = valid ? validColor : invalidColor
    }
	
	func showError(error : OperationError, errorMessage : String) {
		TSNotifier.notify(error == .NetworkError ? "kNoInternetConnection".localized : errorMessage.localized, withAppearance: [kTSNotificationAppearanceTextColor : UIColor.redColor()], onView: self.view)
	}
    
}

class RefreshableBaseViewController : BaseViewController {
    
    enum RefresherMessage {
        case Date(title : String?, format : String?)
        case Custom(String)
    }
    
    /// Color of the UIRefreshControl
    var refresherColor : UIColor {
        return StoqiPallete.mainColor
    }
    
    /// Target UITableView to which refresher will be added.
    var refresherTableView : UITableView? {
        return nil
    }
    
    /// Message type to be displayed on refresher.
    var refresherMessage : RefresherMessage {
        return .Date(title: self.defaultMessageTitle, format: self.defaultMessageDateFormat)
    }
    
    /** Indicates whether the refresher is triggered or not.
     - Note: Setting property to true triggers refresher.
     */
    var refreshing : Bool {
        get { return self.rcRefresh?.refreshing ?? false }
        set {
            guard self.refreshing != newValue && self.rcRefresh != nil else {
                return
            }
            if newValue {
                self.rcRefresh?.beginRefreshing()
            } else {
                self.rcRefresh?.endRefreshing()
            }
        }
    }
    
    /// Updates refresher message with refresherMessage property.
    final func updateRefresher() {
        self.setRefreshTitle(NSDate())
    }
    
    /// Gets called whenever refresher is triggered
    func refresh() {
        print("\(self.dynamicType): refresh() must be overriden to perform custom refreshing.")
    }
    
    private var rcRefresh : UIRefreshControl?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let tableView = self.refresherTableView {
            self.rcRefresh = UIRefreshControl()
            self.rcRefresh?.tintColor = self.refresherColor
            self.rcRefresh?.addTarget(self, action: #selector(refresh), forControlEvents: .ValueChanged)
            self.setRefreshTitle()
            tableView.addSubview(self.rcRefresh!)
        }
    }
    
    private let defaultMessageTitle = "Last Updated"
    private let defaultMessageDateFormat = "MM/dd/yyyy HH:mm:ss"
    
    
    private func setRefreshTitle(date : NSDate? = nil) {
        let message : String
        switch refresherMessage {
        case .Date(let title, let format):
            if let date = date {
                message = "\(title ?? self.defaultMessageTitle) \(date.toString(withFormat:format ?? self.defaultMessageDateFormat))"
            } else {
                message = "\(self.defaultMessageTitle): Never"
            }
            
        case .Custom(let msg):
            message = msg
        }
        self.rcRefresh?.attributedTitle = NSAttributedString(string:message)
    }
}

