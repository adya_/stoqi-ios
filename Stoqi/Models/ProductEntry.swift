struct ProductEntry : Identifiable {
    var id : Int? {
        return self.product.id
    }
    var product : Product
    var units : Int
}

func === (first : ProductEntry, second : ProductEntry) -> Bool {
    return  first.product === second.product &&
        first.units == second.units
}

func !== (first : ProductEntry, second : ProductEntry) -> Bool {
    return !(first === second)
}

