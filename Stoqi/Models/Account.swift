
/// Account used to authorize API calls.
struct Account : Identifiable {
    
    let id : Int?
    let email : String?
    let facebookToken : String?
    
    init(id : Int? = nil, email : String? = nil, facebookToken : String? = nil) {
        self.id = id
        self.facebookToken = facebookToken
        self.email = email
    }
}

private enum ArchiveKeys : String {
    case Id = "kAccountId"
    case Email = "kAccountEmail"
    case Token = "kAccountToken"
}

extension Account : Archivable {
    func archived() -> [String : AnyObject] {
        var dic = [String : AnyObject]()
        dic[ArchiveKeys.Id.rawValue] = id
        dic[ArchiveKeys.Email.rawValue] = email
        dic[ArchiveKeys.Token.rawValue] = facebookToken
        return dic
    }
    
    init?(fromArchive archive: [String : AnyObject]) {
        let id = archive[ArchiveKeys.Id.rawValue] as? Int
        let token = archive[ArchiveKeys.Token.rawValue] as? String
        let email = archive[ArchiveKeys.Email.rawValue] as? String
        self.init(id: id, email: email, facebookToken: token)
    }
}