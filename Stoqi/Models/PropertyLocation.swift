struct PropertyLocation : Identifiable {
    let id : Int?
    let cityName : String
    let regionName : String
}

extension PropertyLocation : Archivable {
    func archived() -> [String : AnyObject] {
        guard let id = id else {
            return [:]
        }
        return [
            ArchiveKeys.Id.rawValue : id,
            ArchiveKeys.City.rawValue : cityName,
            ArchiveKeys.Region.rawValue : regionName
        ]
    }
    
    init?(fromArchive archive: [String : AnyObject]) {
        guard let id = archive[ArchiveKeys.Id.rawValue] as? Int,
        city = archive[ArchiveKeys.City.rawValue] as? String,
        region = archive[ArchiveKeys.Region.rawValue] as? String
        else {
            return nil
        }
        self.init(id: id, cityName: city, regionName: region)
    }
}

private enum ArchiveKeys : String {
    case Id = "kLocationId"
    case City = "kLocationCity"
    case Region = "kLocationRegion"
}