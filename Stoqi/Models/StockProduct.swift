struct StockProduct : Identifiable {
    var id : Int? {
        return product.id
    }
    
    let product : Product
    var left : Float
    let total : Float
}