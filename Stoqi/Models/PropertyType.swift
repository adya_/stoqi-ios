enum PropertyType : Int {
    case Apartment = 1
    case House
    case CommercialRoom
}
