// TODO: Change it to store encrypted ids of the card instead of card info.
struct Card : Identifiable {
    let id: Int?
    let number : String
    let code : String
    let owner : String
    let month : Int
    let year : Int
}