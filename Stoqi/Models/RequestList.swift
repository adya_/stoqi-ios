enum RequestStatus {
    case New(from: NSDate, to: NSDate)
    case Pending(from : NSDate, to : NSDate)
    case Completed(date : NSDate)
}

struct RequestList : Identifiable {
    let id : Int?
    var products : [ProductEntry]? {
        didSet {
           self.updateProductsMetrics()
        }
    }
    var status : RequestStatus
    private(set) var total : Float
    private(set) var count : Int
    
    private mutating func updateProductsMetrics() {
        if let products = self.products {
            self.total = products.reduce(0.0) {$0.0 + $0.1.product.price * Float($0.1.units)}
            self.count = products.count
        }
    }
    
    init(id : Int?, products : [ProductEntry]? = nil, status : RequestStatus, total : Float = 0, count : Int = 0) {
        self.id = id
        self.status = status
        self.total = total
        self.count = count
        self.products = products
        self.updateProductsMetrics()
    }
}