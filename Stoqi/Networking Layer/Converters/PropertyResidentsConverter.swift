class PropertyResidentsConverter : ResponseConverter<PropertyResidents> {
    override func convert(dictionary : [String : AnyObject]) -> PropertyResidents? {
        guard let adults = (dictionary["residentAdults"] as? NSNumber)?.integerValue,
            children = (dictionary["residentChildren"] as? NSNumber)?.integerValue,
            pets = (dictionary["residentPets"] as? NSNumber)?.integerValue
            else {
                return nil
        }
        return PropertyResidents(adults: adults, children: children, pets: pets)
    }
}