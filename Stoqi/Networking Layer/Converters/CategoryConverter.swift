class CategoryConverter : ResponseConverter<Category> {
    override func convert(dictionary: [String : AnyObject]) -> Category? {
        guard let id = (dictionary["id"] as? String).flatMap({Int($0)}),
            name = dictionary["name"] as? String
            else {
                return nil
        }
        
        return Category(id: id, name: name)
    }
}