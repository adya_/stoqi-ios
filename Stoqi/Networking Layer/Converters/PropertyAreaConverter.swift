class PropertyAreaConverter : ResponseConverter<PropertyArea> {
    override func convert(dictionary : [String : AnyObject]) -> PropertyArea? {
        guard let area = (dictionary["propertyArea"] as? NSNumber)?.integerValue,
            rooms = (dictionary["propertyRooms"] as? NSNumber)?.integerValue
            else {
            return nil
        }
        return PropertyArea(area: area, rooms: rooms)
    }
}