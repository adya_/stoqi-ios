class RequestListConverter : ResponseConverter<RequestList> {

    override func convert(dictionary: [String : AnyObject]) -> RequestList? {
        guard let id = (dictionary["id"] as? String).flatMap({Int($0)}),
        total = dictionary["price"] as? Float,
        isDelivered = dictionary["isDelivered"] as? Bool
        else {
            return nil
        }
        let items = dictionary["totalItems"] as? Int ?? 0
        let status : RequestStatus
        if isDelivered {
            guard let delivered = (dictionary["deliveredOn"] as? String).flatMap({NSDate.fromString($0, withFormat: "yyyy-MM-dd HH:mm:ss")}) else {
                return nil
            }
            status = .Completed(date: delivered)
        } else {
            if let requested = (dictionary["requestedOn"] as? String).flatMap({NSDate.fromString($0, withFormat: "yyyy-MM-dd HH:mm:ss")}) {
                let latest = requested + TSDateComponents.Months(1)
                status = .Pending(from: requested, to: latest)
            } else {
                // TODO: Fix New requests
                let requested = NSDate()
                let latest = requested + TSDateComponents.Months(1)
                status = .New(from: requested, to: latest)
            }
            
        }
        
        return RequestList(id: id, products: nil, status: status, total: total, count: items)
    }
}

class RequestedProductConverter : ResponseConverter<ProductEntry> {
    
    private let productConverter : ResponseConverter<Product>
    
    init(productConverter : ResponseConverter<Product>) {
        self.productConverter = productConverter
    }
    
    override func convert(dictionary: [String : AnyObject]) -> ProductEntry? {
        guard let product = self.productConverter.convert(dictionary),
            units = (dictionary["quantity"] as? String).flatMap({Int($0)}) else {
                return nil
        }
        return ProductEntry(product: product, units: units)
    }
}