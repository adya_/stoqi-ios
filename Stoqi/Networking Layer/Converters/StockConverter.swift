class StockConverter : ResponseConverter<Stock> {
    
    private let converter : ResponseConverter<StockProduct>
    
    init(stockProductConverter : ResponseConverter<StockProduct>) {
        self.converter = stockProductConverter
    }
    
    override func convert(dictionary: [String : AnyObject]) -> Stock? {
        guard let categories = dictionary["categories"] as? [[String : AnyObject]] else {
            return nil
        }
        let products = categories.flatMap {self.converter.convert($0)}
        return Stock(products: products)
    }
}

class StockProductConverter : ResponseConverter<StockProduct> {
    
    let productConverter : ResponseConverter<Product>
    
    init(productConverter : ResponseConverter<Product>) {
        self.productConverter = productConverter
    }
    
    override func convert(dictionary: [String : AnyObject]) -> StockProduct? {
        guard let product = self.productConverter.convert(dictionary),
            units = (dictionary["quantityLeft"] as? String).flatMap({Float($0)}),
            total = (dictionary["quantity"] as? String).flatMap({Float($0)}) else {
                return nil
        }
        return StockProduct(product: product, left: units, total: total)
    }
}