class VolumeConverter : ResponseConverter<Volume> {
    override func convert(dictionary: [String : AnyObject]) -> Volume? {
        guard let id = (dictionary["id"] as? String).flatMap({Int($0)}),
            name = dictionary["name"] as? String,
            unit = dictionary["unit"] as? String,
            volume = (dictionary["volume"] as? String).flatMap({Int($0)})
            else {
                return nil
        }
        
        return Volume(id: id, name: name, volume: volume, unit: unit)
    }
}