class CardConverter : ResponseConverter<Card> {
    override func convert(dictionary: [String : AnyObject]) -> Card? {
        guard let name = dictionary["name"] as? String,
                    number = dictionary["number"] as? String,
                month = (dictionary["expitedMonth"] as? String).flatMap({Int($0)}),
                year = (dictionary["expitedYear"] as? String).flatMap({Int($0)}),
				cvv = dictionary["cvv"] as? String
			
        else {
            return nil
        }
        // TODO: parse code
		return Card(id: nil, number: number, code: cvv, owner: name, month: month, year: year)
    }
}