class BrandConverter : ResponseConverter<Brand> {
    override func convert(dictionary: [String : AnyObject]) -> Brand? {
        guard let id = (dictionary["id"] as? String).flatMap({Int($0)}),
            name = dictionary["name"] as? String
            else {
                return nil
        }
        
        return Brand(id: id, name: name)
    }
}