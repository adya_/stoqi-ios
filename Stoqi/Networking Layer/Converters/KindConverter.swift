class KindConverter : ResponseConverter<Kind> {
    override func convert(dictionary: [String : AnyObject]) -> Kind? {
        guard let id = (dictionary["id"] as? String).flatMap({Int($0)}),
            name = dictionary["name"] as? String
            else {
                return nil
        }
        
        return Kind(id: id, name: name)
    }
}