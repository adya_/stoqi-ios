enum AccountResponseValue {
    case ExistingAccount(Account)
    case CreatedAccount(Account)
    case NoAccount
}

struct AccountResponse : Response {
    let request : Request
    let value : AccountResponseValue
    static let kind = ResponseKind.JSON
    
    init?(request: Request, body: AnyObject) {
       self.request = request
        guard let response = (body as? [String : AnyObject])?["response"] as? [String : AnyObject] else {
            return nil
        }
        
        let accountConverter = try! Injector.inject(ResponseConverter<Account>.self)
        let exists = response["exists"] as? Bool ?? false
        
        if let account = accountConverter.convert(response) {
            self.value = exists ? .ExistingAccount(account) : .CreatedAccount(account)
        } else if !exists {
            self.value = .NoAccount
        } else {
            return nil
        }
    }
}