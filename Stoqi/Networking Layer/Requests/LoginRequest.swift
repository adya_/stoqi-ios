struct LoginRequest : Request {
    let method = RequestMethod.GET
    let url = "login"
    let parameters: [String : AnyObject]?
    
    init(credentials : Credentials) {
        self.parameters = ["email" : credentials.email,
                          "password" : credentials.password]
    }
}