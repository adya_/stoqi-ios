struct LoadStockRequest : Request {
    let method = RequestMethod.GET
    let url = "user/loadstock"
    let parameters: [String : AnyObject]?
    
    init?(account : Account) {
        guard let id = account.id else {
            return nil
        }
        parameters = ["accountId" : id]
    }
}