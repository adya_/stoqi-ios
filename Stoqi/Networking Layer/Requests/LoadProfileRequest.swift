struct LoadProfileRequest : Request {
    let method = RequestMethod.GET
    let url = "user/profile"
    let parameters: [String : AnyObject]?
    
    init?(account : Account) {
        guard let accountId = account.id else {
            return nil
        }
        
        self.parameters = ["accountId" : accountId]
    }
}