struct ProcessRequestListRequest : Request {
    let method = RequestMethod.POST
    let url = "user/processRequest"
    let encoding = RequestEncoding.URL
    let parameters: [String : AnyObject]?
    
    init?(requestList : RequestList) {
        guard let id = requestList.id else {
            return nil
        }
        self.parameters = ["requestId" : id]
    }
}