struct RegisterRequest : Request {
    let method = RequestMethod.POST
    let url = "register"
    let encoding = RequestEncoding.URL
    let parameters: [String : AnyObject]?
    
    init(credentials : Credentials) {
        self.parameters = ["email" : credentials.email,
                           "password" : credentials.password]
    }
}