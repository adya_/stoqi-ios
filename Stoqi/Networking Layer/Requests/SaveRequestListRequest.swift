struct SaveRequestListRequest : Request {
    let method = RequestMethod.POST
    let url = "user/saverequest"
    let parameters: [String : AnyObject]?
    
    init?(account : Account, requestList : RequestList) {
        guard let accountId = account.id, reqID = requestList.id  else {
            return nil
        }
        
        let items : [[String : AnyObject]] = requestList.products?.flatMap { product in
            product.id.flatMap {
                ["id" : $0,
                    "price" : product.product.price,
                "quantity" : product.units]
            }
		} ?? []
		
		
		self.parameters = ["accountId" : accountId,
		                   "items" : items,
		                   "requestId" : reqID]
    }
}