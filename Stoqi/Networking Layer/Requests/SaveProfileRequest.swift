struct SaveProfileRequest : Request {
    let method = RequestMethod.POST
    let url = "user/profile"
    let parameters: [String : AnyObject]?
    
    init?(account : Account, profile : Profile) {
        guard let accountId = account.id else {
                return nil
        }
        var params : [String : AnyObject] = [:]
        // Mandatory fields
        params["accountId"] = accountId
        
        // Optional fields
        
        params["propertyArea"] = profile.propertyArea?.area
        params["propertyRooms"] = profile.propertyArea?.rooms
        params["residentAdults"] = profile.propertyResidents?.adults
        params["residentChildren"] = profile.propertyResidents?.children
        params["residentPets"] = profile.propertyResidents?.pets
        
        
        // Address
        if profile.location != nil ||
            profile.propertyType != nil ||
            profile.address != nil ||
			profile.phone != nil {
            var addressParams : [String : AnyObject] = [:]
            addressParams["city"] = profile.location?.id
            addressParams["propertyType"] = profile.propertyType?.rawValue
            addressParams["street"] = profile.address?.street
            addressParams["streetNumber"] = profile.address?.building
            addressParams["phone1"] = profile.phone
            //addressParams["phone2"] = profile.address?.phoneSecondary
            addressParams["zip"] = profile.address?.zip
            addressParams["latitude"] = profile.address?.latitude
            addressParams["longitude"] = profile.address?.longitude
            
            params["address"] = addressParams
        }
        
        // Profile
        if profile.email != nil ||
            profile.name != nil ||
            profile.priority != nil {
            var profileParams : [String : AnyObject] = [:]
            profileParams["name"] = profile.name
            profileParams["email"] = profile.email
            profileParams["userType"] = profile.priority?.rawValue
            
            params["profile"] = profileParams
        }
        
        // Card
        if let card = profile.primaryCard {
            var cardParams : [String : AnyObject] = [:]
            cardParams["name"] = card.owner
            cardParams["number"] = card.number
            cardParams["expityMonth"] = card.month
            cardParams["expityYear"] = card.year
			//cardParams["cvv"] = card.code
            
            params["card"] = cardParams
        }
        
        self.parameters = params
    }
}