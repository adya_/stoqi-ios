struct LoadProductsRequest : Request {
    let method = RequestMethod.GET
    let url = "products"
    let parameters: [String : AnyObject]? = nil
}