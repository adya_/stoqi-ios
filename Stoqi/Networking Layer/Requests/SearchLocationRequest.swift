struct SearchLocationRequest : Request {
    let method = RequestMethod.GET
    let url = "searchlocation"
    let parameters: [String : AnyObject]?
    
    init(term : String) {
        self.parameters = ["term" : term]
    }
}