class RequestsViewController: BaseViewController, UITabBarControllerDelegate {
    
    private static let segOpenList = "segOpenList"
	private static let kStorageOpenFirstList = "kStorageOpenFirstList"
    
    private let manager : RequestsManager = try! Injector.inject(RequestsManager.self)
    
    @IBOutlet weak private var tvOptions: UITableView!
    
    private var requestsVM : [RequestViewModel]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.delegate = self
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        if let requests = self.manager.requests {
            self.initViewModel(requests)
        } else {
            self.loadData()
        }
    }
    
    private func initViewModel(requests : [RequestList]) {
        self.requestsVM = requests.sort {
            if case .New = $0.0.status {
                return true
            } else if case .Pending(let date1, _) = $0.0.status,
                .Pending(let date2, _) = $0.1.status {
                return date1 >= date2
            } else if case .Completed(let date1) = $0.0.status,
                           .Completed(let date2) = $0.1.status {
                return date1 >= date2
            } else {
                return false
            }
            }.flatMap {try? Injector.inject(RequestViewModel.self, with: $0) }
        if self.requestsVM.count > 2 {
            self.requestsVM[1].hasNextNode = false
        }
        self.tvOptions.reloadData()
		if Storage.temp.popObjectForKey(self.dynamicType.kStorageOpenFirstList) as? Bool ?? false == true
		{
			openFirstList()
		}
    }
    
    private func loadData() {
        TSNotifier.showProgressWithMessage("Loading requests..", onView: self.view)
        self.manager.performLoadRequests {
            TSNotifier.hideProgress()
            switch $0 {
            case .Success(let requests):
                self.initViewModel(requests)
			case let .Failure(error):
				
				if error == .NetworkError {
					TSNotifier.notify("kNoInternetConnection".localized, withAppearance: [kTSNotificationAppearanceTextColor : UIColor.redColor()], onView: self.view)
				}
				
                let alert = UIAlertController(title: "Stoqi", message: "Failed to load request history. Please, try again.", preferredStyle: .Alert)
                alert.addAction(UIAlertAction(title: "Try Again", style: .Default, handler: { _ in
                    self.loadData()
                }))
                alert.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: { _ in
                    (self.tabBarController as? StoqiTabBarController)?.openTab(.Main)
                }))
                self.presentViewController(alert, animated: true, completion: nil)
            }
        }
    }

    @IBAction func unwindRequestBuy(segue : UIStoryboardSegue) {
        (self.tabBarController as? StoqiTabBarController)?.openTab(.Main)
    }
    
    @IBAction func unwindRequestCancel(segue : UIStoryboardSegue) { }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        guard let id = segue.identifier where id == self.dynamicType.segOpenList else {
            return
        }
        
        guard let controller = segue.destinationViewController as? RequestListViewController else {
            return
        }

        controller.mode = .Subsequent
    }
	
	func openFirstList()
	{
		guard let table = tvOptions where self.manager.requests != nil else {
			Storage.temp[self.dynamicType.kStorageOpenFirstList] = true
			return
		}
		tableView(table, didSelectRowAtIndexPath: NSIndexPath(forRow: 0, inSection: 0))
	}
}

// MARK: TableView Implementation
extension RequestsViewController :  UITableViewDataSource, UITableViewDelegate {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.requestsVM?.count ?? 0
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return (indexPath.row == 0 ? CurrentRequestCell.height : HistoryRequestCell.height)
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCellOfType(CurrentRequestCell.self)
            cell.configure(with: self.requestsVM[indexPath.row])
            return cell
        } else {
            let cell = tableView.dequeueReusableCellOfType(HistoryRequestCell.self)
            cell.configure(with: self.requestsVM[indexPath.row])
            return cell
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        Storage.temp[kStorageRequestListValue] = requestsVM[indexPath.row].request
        Storage.temp[kStorageRequestListReadonlyFlag] = (indexPath.row > 0)
        self.performSegueWithIdentifier(self.dynamicType.segOpenList, sender: self)
        tableView.deselectRowAtIndexPath(indexPath
            , animated: true)
    }
}