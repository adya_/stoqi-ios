let kStorageRequestListValue = "kRequestListValue"
let kStorageRequestListReadonlyFlag = "kRequestListOnlyFlag"

class RequestListViewController : BaseViewController, UIScrollViewDelegate {
    
    private static let segFinalize = "segFinalize"
    private static let segInitialClose = "segClose"
    private static let segSubsequentClose = "segCancel"
    private static let segEditItem = "segEditItem"
    private static let segAddItem = "segAddItem"
    
    private let manager = try! Injector.inject(RequestsManager.self)
    private let productManager = try! Injector.inject(ProductsManager.self)
    
    private var viewModel : RequestListViewModel!
    
    @IBOutlet weak private var svContent: UIScrollView!
    @IBOutlet weak private var bBack: UIButton!
    @IBOutlet weak private var bClose: UIButton!
    @IBOutlet weak private var bFinalize: UIButton!
    @IBOutlet weak private var tvProducts: ExpandedTableView!
    
    var mode : RequestListViewControllerMode = .Initial
    private var isReadOnly : Bool = false
    
    private var segClose : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let isInitial = mode == .Initial
        segClose = isInitial ? self.dynamicType.segInitialClose : self.dynamicType.segSubsequentClose
        bClose.hidden = !isInitial
        bBack.hidden = isInitial
        
        tvProducts.hidden = true
        guard let request = Storage.temp[kStorageRequestListValue] as? RequestList else {
            print("\(self.dynamicType): RequestList was not set.")
            parentViewController?.dismissViewControllerAnimated(true, completion: nil)
            return
        }
        isReadOnly = Storage.temp.popObjectForKey(kStorageRequestListReadonlyFlag) as? Bool ?? false
        guard request.products != nil else {
            loadProducts(request)
            return
        }
        viewModel = try! Injector.inject(RequestListViewModel.self, with: request)
        updateView()
    }
    
    private func updateView() {
        tvProducts.hidden = false
        tvProducts.reloadData()
		if isReadOnly
		{
			bFinalize.hidden = true
		}
    }
    
    private func loadProducts(request : RequestList) {
        TSNotifier.showProgressWithMessage("Loading products..", onView: view)
        manager.performLoadRequestList(request){
            TSNotifier.hideProgress()
            switch $0 {
            case .Success(let request):
                self.viewModel = try! Injector.inject(RequestListViewModel.self, with: request)
                self.updateView()
			case let .Failure(error):
				
				if error == .NetworkError {
					TSNotifier.notify("kNoInternetConnection".localized, withAppearance: [kTSNotificationAppearanceTextColor : UIColor.redColor()], onView: self.view)
				}
				
                let alert = UIAlertController(title: "Stoqi", message: "Failed to load products. Please, try again.", preferredStyle: .Alert)
                alert.addAction(UIAlertAction(title: "Try Again", style: .Default, handler: { _ in
                    self.loadProducts(request)
                }))
                alert.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: { _ in
                    self.dismissViewControllerAnimated(true, completion: nil)
                }))
                self.presentViewController(alert, animated: true, completion: nil)
            }
        }
    }
	
	func openPurchase()
	{
		saveList(){
			self.performSegueWithIdentifier(self.dynamicType.segFinalize, sender: self)
		}
	}
	
	func saveList(completion: (()->())? = nil)
	{
		TSNotifier.showProgressWithMessage("Saving list..", onView: view)
		manager.performSaveRequest(viewModel.requestList!, callback: {
			TSNotifier.hideProgressOnView(self.view)
			switch $0 {
			case .Success: completion?()
			case let .Failure(error):
				self.showError(error, errorMessage: "Failed to save products list.".localized)
			}
		})
	}

    @IBAction func navAction(sender: UIButton) {
        switch sender {
        case bBack:
            guard !isReadOnly else {
                self.performSegueWithIdentifier(self.dynamicType.segSubsequentClose, sender: self)
                return
            }
			saveList(){
				self.performSegueWithIdentifier(self.dynamicType.segSubsequentClose, sender: self)
			}
        case bFinalize:

			guard (viewModel.categories.count > 0 && !viewModel.categories.reduce(true){ $0 && $1.count == 0 }) else
			{
				TSNotifier.notify("The list is empty", onView: self.view)
				return
			}
			
			openPurchase()
			
			case bClose:
            performSegueWithIdentifier(segClose, sender: self)
        default: break
        }
    }
	
	@IBAction func unwindPurchaseToCards(segue : UIStoryboardSegue)
	{
		if let segue = segue as? TSStoryboardSegue {
			segue.completion = {
				guard let vc = (self.tabBarController as? StoqiTabBarController)?.openTab(StoqiTab.Settings) as? ProfileViewController else {
					return
				}
				vc.openCards()
			}
		}
	}
	
	@IBAction func unwindPurchaseToAddress(segue : UIStoryboardSegue)
	{
		if let segue = segue as? TSStoryboardSegue {
			segue.completion = {
				guard let vc = (self.tabBarController as? StoqiTabBarController)?.openTab(StoqiTab.Settings) as? ProfileViewController else {
					return
				}
				
				vc.openAddress()
			}
		}
	}
	
    @IBAction func unwindPurchaseToList(segue : UIStoryboardSegue) {}
    
    @IBAction func unwindManageItemCancel(segue : UIStoryboardSegue) {}
    
    @IBAction func unwindManageItemSave(segue : UIStoryboardSegue) {
        if let controller = segue.sourceViewController as? EditProductEntryViewController,
            product = controller.product {
            setProduct(product)
        } else if let controller = segue.sourceViewController as? AddProductEntryViewController,
            product = controller.product {
            addProduct(product)
        }
    }
    
    @IBAction func unwindManageItemDelete(segue : UIStoryboardSegue) {
        guard let controller = segue.sourceViewController as? EditProductEntryViewController,
        product = controller.product else {
            return
        }
        removeProduct(product)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let controller = segue.destinationViewController as? PurchaseViewController {
            controller.mode = mode
            controller.request = viewModel.requestList!
        }
    }
    
    // Locks top-edge bouncing
    func scrollViewDidScroll(scrollView: UIScrollView) {
        if scrollView.contentOffset.y <= 0 {
            scrollView.contentOffset = CGPointZero
        }
    }
}

// MARK:- Interactor
extension RequestListViewController {
    
    func addProduct(product : ProductEntry) {
        guard var request = viewModel.requestList else {
            return
        }
        request.products?.append(product)
        viewModel = try! Injector.inject(RequestListViewModel.self, with: request)
        updateView()
    }
    
    func setProduct(product : ProductEntry) {
        guard var request = viewModel.requestList else {
            return
        }
        request.products?[product] = product
        viewModel = try! Injector.inject(RequestListViewModel.self, with: request)
        updateView()
    }
    
    func removeProduct(product : ProductEntry) {
        guard var request = viewModel.requestList else {
            return
        }
        request.products?[product] = nil
        viewModel = try! Injector.inject(RequestListViewModel.self, with: request)
        updateView()
    }
}



// MARK: - Presenter (ViewModel => View)
extension RequestListViewController : UITableViewDataSource, UITableViewDelegate {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return viewModel?.categories.count ?? 0
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let isCollapsed = viewModel.categories[section].collapsed
        return isCollapsed ? 0 : viewModel.categories[section].items.count ?? 0
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return ProductsCategoryHeaderView.height
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableViewOfType(ProductsCategoryHeaderView.self)
        header.configure(with: viewModel.categories[section])
        header.tapped = { _ in
            self.viewModel.categories[section].collapsed = !self.viewModel.categories[section].collapsed
            tableView.reloadSections(NSIndexSet(index: section), withRowAnimation: .Automatic)
        }
        return header
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return isReadOnly || viewModel.categories[section].collapsed ? 0.001 : ProductsCategoryFooterView.height
    }
    
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        guard !isReadOnly else {
            return nil
        }
        let footer = tableView.dequeueReusableViewOfType(ProductsCategoryFooterView.self)
        footer.delegatedAction = {
            Storage.temp[kStorageProductEntryCategory] = self.viewModel.categories[section].category
            self.performSegueWithIdentifier(self.dynamicType.segAddItem, sender: self)
        }
        return footer
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return ProductItemCell.height
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellOfType(ProductItemCell.self)
        cell.configure(with: viewModel.categories[indexPath.section].items[indexPath.row])
        cell.canEdit = !isReadOnly
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        guard !isReadOnly else {
            print("\(self.dynamicType): RequestList is read-only.")
            return
        }
        Storage.temp[kStorageProductEntry] = viewModel.categories[indexPath.section].items[indexPath.row].product
        performSegueWithIdentifier(self.dynamicType.segEditItem, sender: self)
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
}

/// Identifies entry point for the RequestList storyboard to determine navigation flow.
enum RequestListViewControllerMode {
    
    /// Entry point is Registration.
    case Initial
    
    /// Entry point is User RequestList.
    case Subsequent
}