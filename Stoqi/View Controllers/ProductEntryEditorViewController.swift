import UIKit

protocol ProductEntryEditorDelegate {
    func editor(editor : ProductEntryEditor, didEditProductEntry product : ProductViewModel)
}

protocol ProductEntryEditor {
    var viewModel : ProductViewModel! {get}
    var isEdited : Bool {get}
    var delegate : ProductEntryEditorDelegate? {get set}
    
    func setProduct(entry : ProductEntry)
    func setCategory(category : Category)
}

class ProductEntryEditorViewController: StandaloneViewController, ProductEntryEditor {

    private let manager = try! Injector.inject(ProductsManager.self)
    
    @IBOutlet weak private var bSelectProduct: TSOptionButton!
    @IBOutlet weak private var bSelectBrand: TSOptionButton!
    @IBOutlet weak private var bSelectDescription: TSOptionButton!
    @IBOutlet weak private var lUnits: UILabel!
    @IBOutlet weak private var sUnits: UIStepper!
    
    private var kindPickerDataSource : PickerDataSource?
    private var brandPickerDataSource : PickerDataSource?
    private var volumePickerDataSource : PickerDataSource?
  
    private var originalProduct : ProductEntry? = nil
    
    var delegate : ProductEntryEditorDelegate?
    var viewModel : ProductViewModel! {
        didSet {
            if self.isLoaded, let vm = self.viewModel {
                self.configure(with: vm)
                delegate?.editor(self, didEditProductEntry: viewModel)
            }
            
        }
    }
    
    var useAccentTheme : Bool = false
    
    var isEdited : Bool {
        guard let originalProduct = originalProduct else {
            return true
        }
        guard let product = viewModel.productEntry else {
            return false
        }
        return originalProduct !== product
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.configure(with: self.viewModel)
        applyTheme()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        bSelectProduct.circle = true
        bSelectBrand.circle = true
        bSelectDescription.circle = true
    }
    
    private func applyTheme() {
        let color = useAccentTheme ? StoqiPallete.accentColor : StoqiPallete.mainColor
        [bSelectProduct, bSelectBrand, bSelectDescription].forEach {
            $0.setTitleColor(color, forState: .Normal)
            $0.setTitleColor(color, forState: .Selected)
            $0.tintColor = color
        }
        lUnits.textColor = color
   //     sUnits.tintColor = color
    }

    private func configureKindDataSource(withCategory category: Category) {
        let kinds = self.manager.products!.filter{$0.category == category}.map{$0.kind}.distinct
        self.kindPickerDataSource = try! Injector.inject(PickerDataSource.self, with: kinds)
        if let single = kinds.first where kinds.count == 1 {
            self.kindPickerDataSource?.initialValue = try! Injector.inject(PickerItemViewModel.self, with: single)
        }
    }
    
    private func configureBrandDataSource(withKind kind: Kind) {
        let brands = self.manager.products!.filter{$0.kind == kind}
            .map{$0.brand}.distinct
        self.brandPickerDataSource = try! Injector.inject(PickerDataSource.self, with: brands)
        if let single = brands.first where brands.count == 1 {
            self.brandPickerDataSource?.initialValue = try! Injector.inject(PickerItemViewModel.self, with: single)
        }
    }
    
    private func configureVolumeDataSource(withKind kind: Kind, andBrand brand : Brand) {
        let volumes = self.manager.products!.filter{$0.kind == kind && $0.brand == brand}
            .map{$0.volume}.distinct
        self.volumePickerDataSource = try! Injector.inject(PickerDataSource.self, with: volumes)
        if let single = volumes.first where volumes.count == 1 {
            self.volumePickerDataSource?.initialValue = try! Injector.inject(PickerItemViewModel.self, with: single)
        }
    }

}

extension ProductEntryEditorViewController {
    func setProduct(product : ProductEntry) {
        self.viewModel = try! Injector.inject(ProductViewModel.self, with: product)
        self.originalProduct = product
        self.configureKindDataSource(withCategory: product.product.category)
        self.configureBrandDataSource(withKind: product.product.kind)
        self.configureVolumeDataSource(withKind: product.product.kind, andBrand: product.product.brand)
        
        self.kindPickerDataSource?.initialValue = try! Injector.inject(PickerItemViewModel.self, with: product.product.kind)
        self.brandPickerDataSource?.initialValue = try! Injector.inject(PickerItemViewModel.self, with: product.product.brand)
        self.volumePickerDataSource?.initialValue = try! Injector.inject(PickerItemViewModel.self, with: product.product.volume)
    }
    
    func setCategory(category : Category) {
        self.viewModel = try! Injector.inject(ProductViewModel.self, with: category)
        self.configureKindDataSource(withCategory: category)
        self.originalProduct = nil
    }
}

extension ProductEntryEditorViewController {
    // TODO: Optimize picking products
    @IBAction func optionAction(sender: TSOptionButton) {
        switch sender {
        case self.bSelectProduct:
            self.openPicker(self.kindPickerDataSource!, trigger: sender, selectedBlock: { (index, value) in
                self.kindPickerDataSource?.initialValue = value
                let kind = value as! KindPickerItemViewModel
                self.configureBrandDataSource(withKind: kind.kind)
                self.viewModel.kind = kind
            })
        case self.bSelectBrand:
            self.openPicker(self.brandPickerDataSource!, trigger: sender, selectedBlock: { (index, value) in
                self.brandPickerDataSource?.initialValue = value
                let brand = value as! BrandPickerItemViewModel
                self.configureVolumeDataSource(withKind: self.viewModel.kind!.kind, andBrand: brand.brand)
                self.viewModel.brand = brand
            })
        case self.bSelectDescription:
            self.openPicker(self.volumePickerDataSource!, trigger: sender, selectedBlock: { (index, value) in
                self.volumePickerDataSource?.initialValue = value
                self.viewModel.volume = (value as! VolumePickerItemViewModel)
            })
        default: break
        }
    }
    
    @IBAction func unitsChangedAction(sender : UIStepper) {
        self.viewModel.units = Int(sender.value)
    }

}

// MARK: Presenter
extension ProductEntryEditorViewController : TSConfigurable {
    func configure(with dataSource: ProductViewModel) {
        self.bSelectProduct.enabled = !(self.kindPickerDataSource?.values.isEmpty ?? true)
        self.bSelectBrand.enabled = !(self.brandPickerDataSource?.values.isEmpty ?? true)
        self.bSelectDescription.enabled = !(self.volumePickerDataSource?.values.isEmpty ?? true)
        self.bSelectProduct.setTitle(dataSource.kind?.itemText, forState: .Normal)
        self.bSelectBrand.setTitle(dataSource.brand?.name, forState: .Normal)
        self.bSelectDescription.setTitle(dataSource.volume?.itemText, forState: .Normal)
        self.lUnits.text = "\(dataSource.units)"
        self.sUnits.value = Double(dataSource.units)
    }
}
