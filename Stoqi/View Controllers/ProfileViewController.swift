class ProfileViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate, UIPopoverPresentationControllerDelegate {
    
    private enum Segues : String {
        case EditQuestion = "segEditQuestion"
        case ManageCards = "segCards"
		case MyInfo = "segMyInfo"
    }
    
    private enum Sections : Int, CustomStringConvertible {
        case Deliver
        case PayMethod
        case Profile
        
        var description: String {
            switch self {
            case .Deliver: return "Deliver on"
            case .PayMethod: return "Pay method"
            case .Profile: return "Your Profile"
            }
        }
        
        static let values : [Sections] = [.Deliver, .PayMethod, .Profile]
    }
    
    private enum QuestionType : Int {
        case Location
        case Type
        case Area
        case Residents
        case Priority
        
        static let values : [QuestionType] = [.Location, .Type, .Area, .Residents, .Priority]
    }
    
    private let manager = try! Injector.inject(ProfileManager.self)
    
    private var editor : EditAddressViewController!
    
    @IBOutlet weak private var tvProfile: UITableView!
    @IBOutlet weak var lPhone: UILabel!
    @IBOutlet weak var lName: UILabel!
    @IBOutlet weak var lEmail: UILabel!
    @IBOutlet weak var ivPicture: UIImageView!
    
    private var selectedQuestion : QuestionType? {
        didSet {
            if selectedQuestion != nil {
                performSegueWithIdentifier(Segues.EditQuestion.rawValue, sender: self)
            }
        }
    }
    
    private var viewModel : ProfileViewModel! {
        didSet {
            tvProfile.reloadData()
            configure(with: viewModel)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        editor = EditAddressViewController()
        editor.modalPresentationStyle = .Popover
        editor.modalInPopover = true
        editor.preferredContentSize = CGSize(width: 316, height: 168)
        editor.delegate = self
        
        if let profile = manager.profile {
            viewModel = try! Injector.inject(ProfileViewModel.self, with: profile)
        } else {
            viewModel = try! Injector.inject(ProfileViewModel.self)
            loadProfile()
        }
    }
	
	func openCards()
	{
		performSegueWithIdentifier(Segues.ManageCards.rawValue, sender: self)
	}
	
	func openAddress()
	{
		guard let cell = tvProfile.cellForRowAtIndexPath(NSIndexPath(forRow: 0, inSection: Sections.Deliver.rawValue)) else
		{
			return
		}
		
		showPopover(cell)
	}
	
	
	
    
    private func showPopover(base: UIView)
    {
        if let popover = editor.popoverPresentationController {
            popover.delegate = self
            popover.sourceView = base
            popover.sourceRect = base.bounds
            popover.permittedArrowDirections = [.Up, .Down]
            self.presentViewController(editor, animated: true, completion: nil)
        }
    }
    
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return .None
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
	{
		if segue.identifier == Segues.MyInfo.rawValue
		{
			guard let controller = segue.destinationViewController as? ChangeUserInfoViewController, m_profile = manager.profile else {
				return
			}
			
			controller.delegate = self
			controller.setProfile(m_profile)
		}
		
        defer {
            selectedQuestion = nil
        }
        guard let type = selectedQuestion,
            id = segue.identifier where id == Segues.EditQuestion.rawValue,
            let controller = segue.destinationViewController as? QuestionEditor else {
                return
        }
        
        let question = questionForType(type)
        controller.question = question
		
		
    }
	
	func openPurchase()
	{
		guard let navController = (self.tabBarController as? StoqiTabBarController)?.openTab(StoqiTab.Requests) as? UINavigationController, controller = navController.viewControllers.last as? RequestListViewController  else {
			return
		}
		
		controller.openPurchase()
	}
	
	func openYourList()
	{
		guard let navController = (self.tabBarController as? StoqiTabBarController)?.openTab(StoqiTab.Requests) as? UINavigationController, controller = navController.viewControllers.last as? RequestsViewController  else {
			return
		}
		
		controller.openFirstList()
	}
	
	
	func askToOpenYourList()
	{
		let alert = UIAlertController(title: "Stoqi", message: "Your changes impact our recommendations for the next replenishment. Want to check it out now?", preferredStyle: .Alert)
		
		alert.addAction(UIAlertAction(title: "Yes", style: .Default, handler: { _ in
			self.openYourList()
		}))
		alert.addAction(UIAlertAction(title: "No", style: .Default, handler: { _ in
		}))
		self.presentViewController(alert, animated: true, completion: nil)
	}

}

extension ProfileViewController : AddressEditorDelegate {
    func editor(editor: AddressEditor, didEdit viewModel: AddressViewModel) {
		
		saveAddress(viewModel)
		tvProfile.reloadRowsAtIndexPaths([NSIndexPath(forRow: 0, inSection: Sections.Deliver.rawValue)], withRowAnimation: .Automatic)
		dismissViewControllerAnimated(true, completion: nil)
		
		if let _ = Storage.temp.popObjectForKey(kStorageBackToPurchase) as? Bool
		{
			openPurchase()
		}
    }
	
    func editorDidCancel(editor: AddressEditor) {
        dismissViewControllerAnimated(true, completion: nil)
		Storage.temp.popObjectForKey(kStorageBackToPurchase)
    }
}


extension ProfileViewController : UserInfoEditorDelegate {
	func editor(editor : UserInfoEditor, didEdit viewModel : UserInfoViewModel)
	{
		saveUserInfo(viewModel)
	}
	
	func editorDidCancel(editor: UserInfoEditor) {
		
	}
}

// MARK: - Controller
private extension ProfileViewController {
    @IBAction func unwindProfile(segue : UIStoryboardSegue) {
        if let controller = segue.sourceViewController as? ManageCardsViewController,
            card = controller.cards?.first {
            // TODO: Handle multiple cards.
        }
    }
	
	
	@IBAction func unwindProfileToPurchase(segue : UIStoryboardSegue) {
		if let segue = segue as? TSStoryboardSegue {
			segue.completion = {
				self.openPurchase()
			}
		}
	}
	
	
    @IBAction func unwindPrimaryCardSet(segue : UIStoryboardSegue) {
        if let controller = segue.sourceViewController as? ManageCardsViewController, let card = controller.selectedCard {
            viewModel.card = try! Injector.inject(CardViewModel.self, with: card)
            configure(with: viewModel)
        }
		
		if let segue = segue as? TSStoryboardSegue {
			segue.completion = {
				if Storage.temp.popObjectForKey(kStorageBackToPurchase) != nil
				{
					self.openPurchase()
				}
			}
		}
    }
    
    @IBAction func unwindQuestionChanged(segue : UIStoryboardSegue) {
		if let segue = segue as? TSStoryboardSegue {
			segue.completion = {
				guard let editor = segue.sourceViewController as? QuestionEditor where editor.question.isValid else {
					return
				}
				self.saveQuestion(editor.question)
				self.tvProfile.reloadData()
				self.askToOpenYourList()
			}
		}
    }
	
    @IBAction func saveAction(sender: UIButton) {
        saveProfile()
    }
}

// MARK: - Interactor
private extension ProfileViewController {
    func loadProfile() {
        TSNotifier.showProgressWithMessage("Loading Profile..", onView: view)
        manager.performLoadProfile {
            TSNotifier.hideProgress()
            switch $0 {
            case .Success(let profile):
                self.viewModel = try! Injector.inject(ProfileViewModel.self, with: profile)
			case let .Failure(error):
				
				if error == .NetworkError {
					TSNotifier.notify("kNoInternetConnection".localized, withAppearance: [kTSNotificationAppearanceTextColor : UIColor.redColor()], onView: self.view)
				}
				
                let alert = UIAlertController(title: "Stoqi", message: "Failed to load profile. Please, try again.", preferredStyle: .Alert)
                alert.addAction(UIAlertAction(title: "Try Again", style: .Default, handler: { _ in
                    self.loadProfile()
                }))
                alert.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: { _ in
                    (self.tabBarController as? StoqiTabBarController)?.openTab(.Main)
                }))
                self.presentViewController(alert, animated: true, completion: nil)
            }
        }
    }
    
    func saveProfile() {
        if let profile = viewModel?.profile where !isSaved {
            TSNotifier.showProgressWithMessage("Saving Profile..", onView: view)
            TSNotifier.hideProgress()
            manager.performSaveProfile(profile) {
                if case .Success = $0 {
                    TSNotifier.notify("Profile saved!")
				} else if case let .Failure(error) = $0 {
					self.showError(error, errorMessage: "Failed to save profile.".localized)
				}
                self.configure(with: self.viewModel!)
            }
        }
    }
    
    var isSaved : Bool {
        guard let profile = viewModel?.profile else {
            return true
        }
        return !(manager.profile == nil || profile !== manager.profile!)
    }
    
    func saveQuestion(question : QuestionViewModel) {
        if let question = question as? NameQuestionViewModel {
            viewModel.name = question
        } else if let question = question as? PropertyLocationQuestionViewModel {
            viewModel.locationQuestion = question
        } else if let question = question as? PropertyAreaQuestionViewModel {
            viewModel.propertyAreaQuestion = question
        } else if let question = question as? PropertyTypeQuestionViewModel {
            viewModel.propertyTypeQuestion = question
        } else if let question = question as? PropertyResidentsQuestionViewModel {
            viewModel.propertyResidentsQuestion = question
        } else if let question = question as? ProductsPriorityQuestionViewModel {
            viewModel.productsPriorityQuestion = question
        } else {
            print("\(self.dynamicType): Unable to determine QuestionEditor's QuestionViewModel type.")
        }
        if let profile = viewModel.profile {
            UIApplication.sharedApplication().networkActivityIndicatorVisible = true
            manager.performSaveProfile(profile) { _ in
                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
            }
        }
    }
    
    func saveAddress(address : AddressViewModel) {
        viewModel.address = address
        if let profile = viewModel.profile {
            UIApplication.sharedApplication().networkActivityIndicatorVisible = true
            manager.performSaveProfile(profile) { _ in
                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
            }
        }
    }
	
	func saveUserInfo(userInfo: UserInfoViewModel) {
		viewModel.name.name.value = userInfo.name.value
		viewModel.email.value = userInfo.email.value
		viewModel.phone.value = userInfo.phone.value
		
		if let profile = viewModel.profile {
			UIApplication.sharedApplication().networkActivityIndicatorVisible = true
			manager.performSaveProfile(profile) { _ in
				UIApplication.sharedApplication().networkActivityIndicatorVisible = false
			}
		}
	}
}

// MARK: - Presenter
extension ProfileViewController : TSConfigurable {
    func configure(with dataSource: ProfileViewModel) {
        lName.text = dataSource.name.name.value
        lPhone.text = dataSource.phone.value
        lEmail.text = dataSource.email.value
    }
}

// MARK: - TableView
extension ProfileViewController {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return Sections.values.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let section = Sections(rawValue: section) else {
            print("\(self.dynamicType): Couldn't convert section to Sections enum value.")
            return 0
        }
        switch section {
        case .PayMethod: return 1
        case .Deliver: return 1
        case .Profile: return viewModel != nil ? QuestionType.values.count : 0
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        guard let section = Sections(rawValue: indexPath.section) else {
            print("\(self.dynamicType): Couldn't convert section to Sections enum value.")
            return ProfileDeliveryCell.height
        }
        switch section {
        case .Profile: return ProfileQuestionCell.height
        default: return ProfileDeliveryCell.height
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        guard let section = Sections(rawValue: indexPath.section) else {
            print("\(self.dynamicType): Couldn't convert section to Sections enum value.")
            return UITableViewCell()
        }
        switch section {
        case .Deliver:
            let cell = tableView.dequeueReusableCellOfType(ProfileDeliveryCell.self)
            if let address = viewModel.address {
                cell.configure(with: address)
            } else {
                print("\(self.dynamicType): AddressViewModel not defined.")
            }
            return cell
            
        case .PayMethod:
            let cell = tableView.dequeueReusableCellOfType(ProfilePayMethodCell.self)
            if let card = viewModel.card {
                cell.configure(with: card)
            } else {
                print("\(self.dynamicType): CardViewModel not defined.")
            }
            return cell
        case .Profile:
            let cell = tableView.dequeueReusableCellOfType(ProfileQuestionCell.self)
            if let question = questionForIndex(indexPath.row) {
                cell.configure(with: question)
            }
            return cell
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        guard let section = Sections(rawValue: indexPath.section) else {
            print("\(self.dynamicType): Couldn't convert section to Sections enum value.")
            return
        }
        if case .Profile = section {
            selectedQuestion = QuestionType(rawValue: indexPath.row)
        } else if case .PayMethod = section {
            performSegueWithIdentifier(Segues.ManageCards.rawValue, sender: self)
        } else if case .Deliver = section, let cell = tableView.cellForRowAtIndexPath(indexPath) {
            if let address = viewModel.address?.address {
                editor.setAddress(address)
            }
            showPopover(cell)
        }
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    private func questionForIndex(row : Int) -> QuestionViewModel? {
        guard let question = QuestionType(rawValue: row) else {
            return nil
        }
        return questionForType(question)
    }
    
    private func questionForType(question : QuestionType) -> QuestionViewModel {
        switch question {
        case .Location:     return viewModel.locationQuestion
        case .Type:         return viewModel.propertyTypeQuestion
        case .Area:         return viewModel.propertyAreaQuestion
        case .Residents:    return viewModel.propertyResidentsQuestion
        case .Priority:     return viewModel.productsPriorityQuestion
        }
    }

    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return Sections.values[section].description
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let title = self.tableView(tableView, titleForHeaderInSection: section) else {
            print("\(self.dynamicType): Unable to get section title.")
            return nil
        }
        let view = tableView.dequeueReusableViewOfType(CommonHeaderView.self)
        // TODO: Hardcoded style
        let style = try! Injector.inject(CommonHeaderViewStyleSource.self, with: (StoqiPallete.darkGrayTextColor, StoqiPallete.lightColor))
        let dataSource = try! Injector.inject(CommonHeaderViewDataSource.self, with: title)
        view.configure(with: dataSource)
        view.style(with: style)
        return view
    }
    
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CommonHeaderView.height
    }
}

