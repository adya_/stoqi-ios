import UIKit

class IntroViewController: TSPageViewController, TSPageViewControllerDelegate {
    
    private let pageIdentifiers = ["introPage1", "introPage2", "introPage3", "introPage4", "introPage5", "introPage6", "introPage7"]
    
    @IBOutlet weak var vToolbar: UIView!
    @IBOutlet weak private var bSkip: UIButton!
    
    override func viewDidLoad() {
        self.setPages(withIdentifiers: pageIdentifiers)
        self.pageControllerDelegate = self
        self.pageControl?.delegate = self
        self.pageControl?.indicatorViewType = .Color(colors:(defaultColor: StoqiPallete.lightColor, activeColor:StoqiPallete.darkColor))
        super.viewDidLoad()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBarHidden = true
        self.showPage(atIndex: 0)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBarHidden = false
    }
    
    func pageController(pageController: TSPageViewController, didShowViewController controller: UIViewController, forPageAtIndex index: Int) {
        let title = (index == pageIdentifiers.count - 1 ? "Close" : "Skip Intro")
        self.bSkip.setTitle(title, forState: .Normal)
    }
    
    func pageController(pageController: TSPageViewController, instantiateViewControllerWithIdentifier identifier: String) -> UIViewController {
        return self.storyboard!.instantiateViewControllerWithIdentifier(identifier)
    }
    
    override func pageControl(pageControl : TSPageControl, customizeIndicatorView view : UIView, atIndex index : Int) {
        view.circle = true
    }
    
    @IBAction func unwindToStart(segue : UIStoryboardSegue) {}
} 