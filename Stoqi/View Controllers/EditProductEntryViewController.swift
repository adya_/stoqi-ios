let kStorageProductEntry = "kStorageProductEntryValue"

class EditProductEntryViewController : EmbedingViewController, ProductEntryEditorDelegate {
    
    private static let segSaveProduct = "segSaveProduct"
    private static let segDeleteProduct = "segDeleteProduct"
    private static let segCancel = "segCancel"

    private let manager = try! Injector.inject(ProductsManager.self)

    @IBOutlet weak private var bDelete: UIButton!
    @IBOutlet weak private var bSave: UIButton!
    
    private var editor : ProductEntryEditor!
    
    var product : ProductEntry? {
        guard let entry = editor?.viewModel?.productEntry else {
            return nil
        }
        let product = entry.product
        return manager.products?.filter {
            $0.brand == product.brand &&
                $0.kind == product.kind &&
                $0.volume == product.volume
            }.first.flatMap { ProductEntry(product: $0, units: entry.units) }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        editor = embed(ProductEntryEditorViewController.self)
        editor.delegate = self
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        guard let product = Storage.temp[kStorageProductEntry] as? ProductEntry else {
            print("\(self.dynamicType): Product entry not found.")
            return
        }
        editor.setProduct(product)
    }

    func editor(editor: ProductEntryEditor, didEditProductEntry product: ProductViewModel) {
        self.bSave.enabled = product.isValid
    }
}

// MARK: Controller
extension EditProductEntryViewController {
    
    @IBAction func saveAction(sender: UIButton) {
        self.saveProduct()
    }
    
    @IBAction func deleteAction(sender: UIButton) {
        self.deleteProduct()
    }
}

// MARK: Interactor
extension EditProductEntryViewController {
    func saveProduct() {
        guard var product = self.editor.viewModel?.productEntry else {
            TSNotifier.notify("Please, fill in all product details", withAppearance: [kTSNotificationAppearanceTextColor : UIColor.redColor()])
            return
        }

        guard self.editor.isEdited else {
            self.performSegueWithIdentifier(self.dynamicType.segCancel, sender: self)
            return
        }
        
        guard let registeredProduct = self.manager.products?.filter({$0 === product.product}).first else {
            return
        }
        product.product = registeredProduct
        TSNotifier.notify("Product saved!")
        self.performSegueWithIdentifier(self.dynamicType.segSaveProduct, sender: self)
    }
    
    func deleteProduct() {
        TSNotifier.notify("Product removed!")
        self.performSegueWithIdentifier(self.dynamicType.segDeleteProduct, sender: self)
    }
}