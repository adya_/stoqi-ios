class AnalysisViewController: BaseViewController {
    
    @IBOutlet weak var lFeature1: UILabel!
    @IBOutlet weak var lFeature2: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidLayoutSubviews() {
        self.lFeature2.font = self.lFeature1.font
    }
}
