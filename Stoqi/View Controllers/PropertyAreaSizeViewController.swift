class PropertyAreaViewController : StandaloneViewController, QuestionEditor {
   
    @IBOutlet weak private var lArea: UILabel!
    @IBOutlet weak private var lRooms: UILabel!
    @IBOutlet weak private var bKnownSize: UIButton!
    @IBOutlet weak private var sArea: UIStepper!
    @IBOutlet weak private var sRooms: UIStepper!
    @IBOutlet weak private var lRoomsLabel: UILabel!
    
    var delegate: QuestionEditorDelegate?
    var question : QuestionViewModel {
        get {
            return viewModel
        }
        set {
            viewModel = newValue as! PropertyAreaQuestionViewModel
        }
    }
    
    private var viewModel : PropertyAreaQuestionViewModel! {
        didSet {
            guard isViewLoaded() else {
                return
            }
            self.configure(with: viewModel)
            delegate?.questionEditor(self, didEditQuestion: viewModel)
        }
    }
    
    override func viewWillAppear(animated : Bool) {
        super.viewWillAppear(animated)
        configure(with: viewModel)
    }
}


// MARK: - Presenter (ViewModel => View)
extension PropertyAreaViewController : TSConfigurable {
    func configure(with dataSource: PropertyAreaQuestionViewModel) {
        self.lArea.text = (dataSource.knownSize.value ?? true) ? "\(dataSource.size.value ?? 0) m²" : "--"
        self.lRooms.text = "\(dataSource.rooms.value ?? 0)"
        self.lRoomsLabel.text = (dataSource.rooms.value ?? 0) == 1 ? "Room" : "Rooms"
        self.bKnownSize.selected = !(dataSource.knownSize.value ?? true)
        self.sArea.value = Double(dataSource.size.value ?? 0)
        self.sRooms.value = Double(dataSource.rooms.value ?? 0)
        self.sArea.enabled = dataSource.knownSize.value ?? true
    }
}

// MARK: - Controller (View => ViewModel)
private extension PropertyAreaViewController {
    @IBAction func unknownAreaTouched(sender: UIButton) {
        self.viewModel.knownSize.value = !(self.viewModel.knownSize.value ?? true)
    }
    
    @IBAction func stepperChanged(sender: UIStepper) {
        switch sender {
        case self.sArea: self.viewModel.size.value = Int(sender.value)
        case self.sRooms: self.viewModel.rooms.value = Int(sender.value)
        default: break
        }
    }
}