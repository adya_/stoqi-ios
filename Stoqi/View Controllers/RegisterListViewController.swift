import UIKit

class RegisterListViewController : TSPageViewController {
    
    typealias `Self` = RegisterListViewController
    private static let segCreateList = "segCreateList"
	private static let segLogin = "segToLogin"
	
    
    private let manager = try! Injector.inject(ProfileManager.self)
    
    typealias QuestionPage = (controller : StandaloneViewController.Type,
                            question : () -> QuestionViewModel)
    
    private var pages : [QuestionPage]!
    
    @IBOutlet weak private var bNext: UIButton!
    @IBOutlet weak private var bBack: UIButton!
    @IBOutlet weak private var lTitle: UILabel!
    
    
    var viewModel : ProfileViewModel! {
        didSet {
            update()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        pageControllerDelegate = self
        pageControl?.indicatorViewType = TSPageControlIndicatorType.Custom(delegate: { (index) -> UIView in
            let view = RegisterListIndicatorView(title: String(index + 1), defaultColor: UIColor.clearColor(), activeColor: StoqiPallete.mainLightColor)
            view.textColor = UIColor.whiteColor()
            view.textAlignment = .Center
            return view
        })
        pages = [
            QuestionPage(controller: UserNameViewController.self, question: {self.viewModel.name}),
            QuestionPage(controller: PropertyLocationViewController.self,
                question: {self.viewModel.locationQuestion}),
            QuestionPage(controller: PropertyTypeViewController.self,
                question: {self.viewModel.propertyTypeQuestion}),
            QuestionPage(controller: PropertyAreaViewController.self,
                question: {self.viewModel.propertyAreaQuestion}),
            QuestionPage(controller: PropertyResidentsViewController.self,
                question: {self.viewModel.propertyResidentsQuestion}),
            QuestionPage(controller: ProductsPriorityViewController.self,
                question: {self.viewModel.productsPriorityQuestion})
        ]
        if let profile = manager.profile {
            viewModel = try! Injector.inject(ProfileViewModel.self, with: profile)
        } else {
            viewModel = try! Injector.inject(ProfileViewModel.self)
        }

        setPages(withIdentifiers: pages.map{$0.controller.identifier})
        checkCachedQuestions()
    }	
    
    private func update() {
        guard let id = currentIdentifier,
            dataSource = pages.filter({$0.controller.identifier == id}).first?.question() else {
            print("Couldn't get question.")
            return
        }
        configure(with: dataSource)
    }
    
    override func pageControl(pageControl: TSPageControl, customizeIndicatorView view: UIView, atIndex index: Int) {
        view.circle = true
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        guard let id = segue.identifier where id == Self.segCreateList else {
            return
        }
        
        guard let controller = segue.destinationViewController as? ListCreationViewController else {
            return
        }
    
        guard let profile = viewModel.profile else {
            print("Profile is not valid.")
            return
        }
        controller.profile = profile
    }
    
    private func checkCachedQuestions() {
        if !viewModel.name.isValid {
            showPage(withIdentifier: UserNameViewController.identifier)
        } else if !viewModel.locationQuestion.isValid {
            showPage(withIdentifier: PropertyLocationViewController.identifier)
        } else if !viewModel.propertyTypeQuestion.isValid {
            showPage(withIdentifier: PropertyTypeViewController.identifier)
        } else if !viewModel.propertyAreaQuestion.isValid {
            showPage(withIdentifier: PropertyAreaViewController.identifier)
        } else if !viewModel.propertyResidentsQuestion.isValid {
            showPage(withIdentifier: PropertyResidentsViewController.identifier)
        } else {
            showPage(withIdentifier: ProductsPriorityViewController.identifier)
        }
    }
    
    func saveQuestion(question : QuestionViewModel) {
        if let question = question as? NameQuestionViewModel {
            viewModel.name = question
        } else if let question = question as? PropertyLocationQuestionViewModel {
            viewModel.locationQuestion = question
        } else if let question = question as? PropertyAreaQuestionViewModel {
            viewModel.propertyAreaQuestion = question
        } else if let question = question as? PropertyTypeQuestionViewModel {
            viewModel.propertyTypeQuestion = question
        } else if let question = question as? PropertyResidentsQuestionViewModel {
            viewModel.propertyResidentsQuestion = question
        } else if let question = question as? ProductsPriorityQuestionViewModel {
            viewModel.productsPriorityQuestion = question
        } else {
            print("\(self.dynamicType): Unable to determine QuestionEditor's QuestionViewModel type.")
        }
    }
}

extension RegisterListViewController : QuestionEditorDelegate {
    func questionEditor(editor: QuestionEditor, didEditQuestion question: QuestionViewModel) {
        saveQuestion(question)
        configure(with: question)
    }
}

// MARK: - Controller
extension RegisterListViewController {
	
	@IBAction func unwindRegistration(segue : UIStoryboardSegue) {
	}
	
    @IBAction private func back(sender: UIButton) {
		if currentIndex == 0
		{
			performSegueWithIdentifier(Self.segLogin, sender: self)
		}
		else
		{
			showPrevPage()
		}
    }
    
    @IBAction private func nextQuestion(sender: AnyObject) {
        if let profile = viewModel?.profile {
            manager.cacheProfileRegistration(profile)
        }
        if currentIndex == pages.count - 1 {
            performSegueWithIdentifier(Self.segCreateList, sender: self)
        } else {
            showNextPage()
        }
    }
}

// MARK: - Presenter
extension RegisterListViewController : TSConfigurable {
    func configure(with dataSource: QuestionViewModel) {
        let nextTitle : String
        switch currentIndex {
        case pages.count - 1: nextTitle = "Create my list"
        case pages.count - 2: nextTitle = "Last Question"
        default: nextTitle = "Next Question"
        }
        lTitle.text = dataSource.question
        bNext.setTitle(nextTitle, forState: .Normal)
		if currentIndex == pages.count - 1
		{
			bNext.enabled = pages.reduce(true){
				$0 && $1.question().isValid
			}
		}
		else
		{
			bNext.enabled = dataSource.isValid
		}
    }
}

// MARK: - Page Controller customization.
extension RegisterListViewController : TSPageViewControllerDelegate {
    
    func pageController(pageController: TSPageViewController, instantiateViewControllerWithIdentifier identifier: String) -> UIViewController {
        let controller : UIViewController
        if identifier == PropertyLocationViewController.identifier {
            controller = UIStoryboard(name: identifier, bundle: NSBundle.mainBundle()).instantiateInitialViewController()!
        } else {            
            controller = pages.filter{$0.controller.identifier == identifier}.first!.controller.init()
        }
        
        if let page = controller as? QuestionEditor {
            page.delegate = self
            page.question = pages.filter{$0.controller.identifier == identifier}.first!.question()
        }
        return controller
    }
    
    func pageController(pageController: TSPageViewController, didShowViewController controller: UIViewController, forPageAtIndex index: Int) {
        update()
    }
    
   
}