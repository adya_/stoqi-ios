import GCDKit

class ListCreationViewController : BaseViewController {
    
    typealias Static = ListCreationViewController
    private static let segDone = "segDone"
    
    @IBOutlet weak private var bTryAgain: UIButton!
    @IBOutlet weak private var aiSaving: UIActivityIndicatorView!
    
    private let manager = try! Injector.inject(ProfileManager.self)
    
    var profile : Profile!
    
    private var isError : Bool = false {
        didSet {
            if !self.isError {
                self.aiSaving?.startAnimating()
            } else {
                self.aiSaving?.stopAnimating()
            }
            self.bTryAgain?.hidden = !self.isError
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.saveProfile()
    }
}

// MARK: - Controller (ViewModel <=> DataManager)
private extension ListCreationViewController {
    private func saveProfile() {
        self.isError = false
		if self.profile != nil
		{
			self.manager.performCreateProfile(self.profile) {
				if case .Success = $0 {
					self.manager.clearCache()
					self.performSegueWithIdentifier(Static.segDone, sender: self)
				} else if case let .Failure(error) = $0 {
					self.isError = true
					self.showError(error, errorMessage: "Failed to save your list.".localized)
				}
			}
		}
		else
		{
			self.isError = true
			TSNotifier.notify("Failed to save your list.")
		}
	}
}

// MARK: - Interactor (View => ViewModel)
extension ListCreationViewController {
    @IBAction func tryAgainAction(sender: UIButton) {
        self.saveProfile()
    }
}