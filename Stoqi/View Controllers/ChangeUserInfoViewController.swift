protocol UserInfoEditorDelegate {
	func editor(editor : UserInfoEditor, didEdit viewModel : UserInfoViewModel)
	func editorDidCancel(editor : UserInfoEditor)
}

protocol UserInfoEditor {
	var viewModel : UserInfoViewModel! {get}
	var delegate : UserInfoEditorDelegate? {get set}
	
	func setProfile(profile : Profile)
}



class ChangeUserInfoViewController: BaseViewController, UserInfoEditor {
	
	@IBOutlet weak private var tfName: TSTextField!
	@IBOutlet weak private var tfEmail: TSTextField!
	@IBOutlet weak private var tfPhone: TSTextField!
	@IBOutlet weak private var bChange: UIButton!
	
	static let segToProfile = "segToProfile"
	
	var viewModel: UserInfoViewModel!
	var delegate: UserInfoEditorDelegate?
	
	
	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)
		
		if viewModel != nil {
			configure(with: viewModel)
		}
	}
	
	func setProfile(profile: Profile) {
		viewModel = try! Injector.inject(UserInfoViewModel.self, with: profile)
		if isViewLoaded() {
			configure(with: viewModel)
		}
	}

}


extension ChangeUserInfoViewController
{
	@IBAction func actionBack(sender: AnyObject)
	{
		self.delegate?.editorDidCancel(self)
	}
	
	
	@IBAction func actionChangeInfo(sender: AnyObject) {
		view.endEditing(true)
		if viewModel.isValid {
			delegate?.editor(self, didEdit: viewModel)
			performSegueWithIdentifier(ChangeUserInfoViewController.segToProfile, sender: self)
		} else {
			highlightErrors()
			TSNotifier.notify("Please, enter valid values",
			                  withAppearance: [kTSNotificationAppearancePositionYOffset : 16],
			                  onView: presentingViewController?.view ?? view)
		}
	}
	
	@IBAction func editedAction(sender: TSTextField) {
		switch sender {
		case tfName: viewModel.name.value = sender.text
		case tfEmail: viewModel.email.value = sender.text
		case tfPhone: viewModel.phone.value = sender.text
		default:
			break
		}
		setView(sender, valid: true)
	}

}

extension ChangeUserInfoViewController : TSConfigurable {
	func configure(with dataSource: UserInfoViewModel) {
		tfName.text = dataSource.name.value
		tfEmail.text = dataSource.email.value
		tfPhone.text = dataSource.phone.value
	}
	
	func highlightErrors() {
		[(tfName,   viewModel?.name.isValid),
			(tfEmail, viewModel?.email.isValid),
			(tfPhone,      viewModel?.phone.isValid)].forEach {
				setView($0.0, valid: $0.1 ?? true)
		}
	}
	
	func resetErrors() {
		[tfName, tfEmail, tfPhone].forEach {
			setView($0, valid: true)
		}
	}
}

