class StoqiListViewController : BaseViewController, UIPopoverPresentationControllerDelegate {
    
    private static let segBack = "segBack"
    private static let segAddItem = "segAddItem"
    
    @IBOutlet weak private var tvProducts: UITableView!
    @IBOutlet weak private var lDate: UILabel!
    
    private let manager = try! Injector.inject(StockManager.self)
    
    private var editor : EditStockProductViewController!
    private var selectedIndexPath : NSIndexPath?
    
    
    private var viewModel : StockViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        editor = EditStockProductViewController()
        editor.modalPresentationStyle = .Popover
        editor.modalInPopover = true
        editor.preferredContentSize = CGSize(width: 300, height: 140)
        editor.delegate = self
        
        self.tvProducts.hidden = true
        guard let stock = self.manager.stock else {
            self.loadStock()
            return
        }
        self.viewModel = try! Injector.inject(StockViewModel.self, with: stock)
        configure(with: viewModel)
    }
	
	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)
		self.loadStock()
	}
    
    @IBAction func unwindAddItemSave(segue : UIStoryboardSegue) {
        if let controller = segue.sourceViewController as? AddProductEntryViewController,
            product = controller.product {
            addProduct(StockProduct(product: product.product,
                left: Float(product.units),
                total: Float(product.units)))
        }
    }
    
    @IBAction func unwindAddItemCancel(segue : UIStoryboardSegue) {
        
    }
    
    
    
    // Locks top-edge bouncing
    func scrollViewDidScroll(scrollView: UIScrollView) {
        if scrollView.contentOffset.y <= 0 {
            scrollView.contentOffset = CGPointZero
        }
    }
    
    func showPopover(base: UIView)
    {
        if let popover = editor.popoverPresentationController {
            popover.delegate = self
            popover.sourceView = base
            popover.sourceRect = base.bounds
            popover.permittedArrowDirections = [.Up, .Down]
            popover.backgroundColor = StoqiPallete.darkColor
            self.presentViewController(editor, animated: true, completion: nil)
        }
    }
    
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return .None
    }
}

extension StoqiListViewController : StockProductEditorDelegate {
    func editor(editor: StockProductEditor, didEdit viewModel: StockProductViewModel) {
        guard let indexPath = selectedIndexPath else {
            return
        }
        selectedIndexPath = nil
        self.viewModel.categories[indexPath.section].items[indexPath.row] = viewModel
        tvProducts.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .None)
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func editorDidCancel(editor: StockProductEditor) {
        dismissViewControllerAnimated(true, completion: nil)
    }
}

// MARK: - Controller
private extension StoqiListViewController {
    
    @IBAction func backAction(sender: UIButton) {
        self.saveStock()
        self.performSegueWithIdentifier(self.dynamicType.segBack, sender: self)
    }
}

// MARK: - Interactor 
private extension StoqiListViewController {
    func loadStock() {
        TSNotifier.showProgressWithMessage("Loading stock..", onView: self.view)
        self.manager.performLoadStock(){
            TSNotifier.hideProgressOnView(self.view)
            switch $0 {
            case .Success(let stock):
                self.viewModel = try! Injector.inject(StockViewModel.self, with: stock)
                self.configure(with: self.viewModel)
            case .Failure:
                let alert = UIAlertController(title: "Stoqi", message: "Failed to load your stock. Please, try again.", preferredStyle: .Alert)
                alert.addAction(UIAlertAction(title: "Try Again", style: .Default, handler: { _ in
                    self.loadStock()
                }))
                alert.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: { _ in
                    self.dismissViewControllerAnimated(true, completion: nil)
                }))
                self.presentViewController(alert, animated: true, completion: nil)
            }
        }
    }
    
    func saveStock() {
        guard let stock = viewModel.stock else {
            print("Stock not available.")
            return
        }
        TSNotifier.showProgressWithMessage("Saving stock..", onView: self.view)
        self.manager.performSaveStock(stock) {
            TSNotifier.hideProgressOnView(self.view)
            if case .Failure = $0 {
                let alert = UIAlertController(title: "Stoqi", message: "Failed to save your stock. Please, try again.", preferredStyle: .Alert)
                alert.addAction(UIAlertAction(title: "Try Again", style: .Default, handler: { _ in
                     self.saveStock()
                }))
                alert.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: { _ in
                    self.dismissViewControllerAnimated(true, completion: nil)
                }))
                self.presentViewController(alert, animated: true, completion: nil)
            }
        }
    }
    
    func addProduct(product : StockProduct) {
        guard let selectedIndexPath = selectedIndexPath else {
            return
        }
        let productViewModel = try! Injector.inject(StockProductViewModel.self, with: product)
        
        
        self.selectedIndexPath = nil
        viewModel.categories[selectedIndexPath.section].items.append(productViewModel)
        tvProducts.beginUpdates()
        tvProducts.reloadSections(NSIndexSet(index: selectedIndexPath.section), withRowAnimation: .Automatic)
        tvProducts.endUpdates()
    }
}

// MARK: - Presenter
extension StoqiListViewController  : TSConfigurable {
    func configure(with dataSource: StockViewModel) {
        self.lDate.text = "Today, \(dataSource.date.toString(withFormat: "dd MMM yyyy"))"
        self.tvProducts.hidden = dataSource.categories.isEmpty
        self.tvProducts.reloadData()
    }
}


extension StoqiListViewController : UITableViewDelegate, UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.viewModel?.categories.count ?? 0
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let isCollapsed = viewModel.categories[section].collapsed
        return isCollapsed ? 0 : self.viewModel.categories[section].items.count ?? 0
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return ProductsCategoryHeaderView.height
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableViewOfType(ProductsCategoryHeaderView.self)
        header.configure(with: self.viewModel.categories[section])
        header.tapped = { _ in
            self.viewModel.categories[section].collapsed = !self.viewModel.categories[section].collapsed
            tableView.reloadSections(NSIndexSet(index: section), withRowAnimation: .Automatic)
        }
        return header
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return viewModel.categories[section].collapsed ? 0.001 :ProductsCategoryFooterView.height
    }
    
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footer = tableView.dequeueReusableViewOfType(ProductsCategoryFooterView.self)
        footer.delegatedAction = {
            self.selectedIndexPath = NSIndexPath(forRow: 0, inSection: section)
            let category = self.viewModel.categories[section].category
            Storage.temp[kStorageProductEntryCategory] = category
            self.performSegueWithIdentifier(self.dynamicType.segAddItem, sender: self)
        }
        return footer
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return StockProductItemCell.height
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellOfType(StockProductItemCell.self)
        cell.configure(with: self.viewModel.categories[indexPath.section].items[indexPath.row])
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if let cell = tableView.cellForRowAtIndexPath(indexPath) {
            selectedIndexPath = indexPath
            let product = viewModel.categories[indexPath.section].items[indexPath.row].product
            editor.setProduct(product)
            showPopover(cell)
            tableView.deselectRowAtIndexPath(indexPath, animated: true)
        }
    }

}