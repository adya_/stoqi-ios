import UIKit
import GCDKit

class LoginViewController: BaseViewController {
    
    private static let segLogin = "segLogin"
    private static let segRegister = "segRegister"
  
    private let manager = try! Injector.inject(AuthorizationManager.self)
    private let productsManager = try! Injector.inject(ProductsManager.self)
    private let profileManager = try! Injector.inject(ProfileManager.self)
	private let requestsManager = try! Injector.inject(RequestsManager.self)
    
    @IBOutlet weak private var tfLogin: UITextField!
    @IBOutlet weak private var tfPassword: UITextField!
    
    private var viewModel : LoginViewModel!
    
    override func viewWillAppear(animated : Bool) {
        super.viewWillAppear(animated)
        self.viewModel = try! Injector.inject(LoginViewModel.self)
        self.configure(with: self.viewModel)
        self.navigationController?.navigationBarHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if manager.account != nil {
            proceed()
        }
    }
    
    func proceed() {
        self.prepareStoqi() {
            if self.profileManager.checkProfileRegistered() {
                self.performSegueWithIdentifier(self.dynamicType.segLogin, sender: self)
            } else {
               self.performSegueWithIdentifier(self.dynamicType.segRegister, sender: self)
            }
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        guard let id = segue.identifier where id == self.dynamicType.segRegister else {
            return
        }
        
        guard let controller = segue.destinationViewController as? RequestListViewController else {
            return
        }
        
        controller.mode = .Initial
    }
    
	private func showConfirmation(withRecovery: Bool) {
		
		let controller = UIAlertController(title: "Authorization", message: withRecovery ? "Password doesn't match entered email" : "Hello, we haven't recognized you. Please, confirm that you want to join Stoqi.", preferredStyle: .Alert)
        
		
		if withRecovery
		{
			controller.addAction(UIAlertAction(title: "Recover Password", style: .Default) { _ in
				self.recoverPassword()
				})
		}
		else
		{
			controller.addAction(UIAlertAction(title: "Sign Up", style: .Default) { _ in
				self.register()
				})
		}

        controller.addAction(UIAlertAction(title: "Oops, let me check!", style: .Cancel) { _ in })
        
        self.presentViewController(controller, animated: true, completion: nil)
    }
}

// MARK: - Controller
private extension LoginViewController {

    @IBAction func fieldsEdited(sender: UITextField) {
        switch sender {
        case tfLogin: self.viewModel.login.value = sender.text
        case tfPassword: self.viewModel.password.value = sender.text
        default: break
        }
    }
    
    @IBAction func loginAction(sender: UIButton) {
        self.view.endEditing(true)
        self.login()
    }
    
    @IBAction func unwindLogout(segue : UIStoryboardSegue) {
        self.manager.performLogout()
    }
    
    @IBAction func facebookAction(sender: UIButton) {
        view.endEditing(true)
        loginFacebook()
    }
    
    @IBAction func unwindToLogin(segue : UIStoryboardSegue) {
    }

}

// MARK: - Interactor
private extension LoginViewController {
    func login() {
        guard self.viewModel.isValid, let credentials = self.viewModel.credentials else {
            TSNotifier.notify(viewModel.validationErrors.first, withAppearance: [kTSNotificationAppearanceTextColor : UIColor.redColor()])
            return
        }
        
        TSNotifier.showProgressWithMessage("Logging in...".localized, onView: self.view)
        self.manager.performAuthorization(withCredentials: credentials) {
            TSNotifier.hideProgress()
            if case .Authorized = $0
			{
				self.proceed()
            }
            else if case .NotExisted = $0 {
                self.showConfirmation(false)
            }
            else if case let .Failure(error) = $0 {
				
				if error == .InvalidResponse
				{
					self.showConfirmation(true)
				}
				else
				{
					self.showError(error, errorMessage: "kLoginFailed".localized)
				}
            }
        }
    }
	
	func recoverPassword()
	{
		guard self.viewModel.login.isValid, let email = self.viewModel.login.value  else {
			return
		}
		
		TSNotifier.showProgressWithMessage("Recovering...".localized, onView: self.view)
		self.manager.performPasswordRecovery(email) {
			TSNotifier.hideProgressOnView(self.view)
			if case .Success = $0
			{
				TSNotifier.notify("We've sent you an email".localized, onView: self.view)
			}
			else if case let .Failure(error) = $0 {
				self.showError(error, errorMessage: "Sorry. Try again later.".localized)
			}
		}
	}
    
    func register() {
        guard self.viewModel.isValid, let credentials = self.viewModel.credentials else {
            TSNotifier.notify("kEnterValidEmailAndPassword".localized, withAppearance: [kTSNotificationAppearanceTextColor : UIColor.redColor()])
            return
        }
        
        TSNotifier.showProgressWithMessage("Logging in...".localized, onView: self.view)
        self.manager.performRegistration(withCredentials: credentials) {
            TSNotifier.hideProgressOnView(self.view)
            if case .Created = $0 {
                self.proceed()
			} else if case let .Failure(error) = $0 {
				self.showError(error, errorMessage: "Sorry, login failed. Try again later.".localized)
			}
        }
    }
    
    func loginFacebook() {
        FacebookHelper.authorizeFacebook(fromViewController: self) {
            guard case let .Authorized(token) = $0 else {
                if case .Canceled = $0 {
                    print("Facebook authorization canceled.")
					TSNotifier.notify("To get access by Facebook we need permissions.", withAppearance: [kTSNotificationAppearanceTextColor : UIColor.redColor()], onView: self.view)
                } else {
                    print("Facebook authorization failed!")
					TSNotifier.notify("Facebook authorization failed!", withAppearance: [kTSNotificationAppearanceTextColor : UIColor.redColor()], onView: self.view)
                }
                return
            }
            TSNotifier.showProgressWithMessage("Logging in..", onView: self.view)
            self.manager.performFacebookAuthorization(token) {
                if case .Authorized = $0 {
                    TSNotifier.hideProgress()
                    self.proceed()
                }
                else if case .Created = $0 {
                    self.loadFacebookUser()
                }
				else  if case let .Failure(error) = $0 {
					TSNotifier.hideProgress()
					self.showError(error, errorMessage: "Sorry, login failed. Try again later.".localized)
				}
            }
        }
    }
    
    func loadFacebookUser() {
        FacebookHelper.getUser {
            if case .Success(let (name, email)) = $0 {
                let profile = Profile(name : name, email: email)
                self.profileManager.performSaveProfile(profile) { _ in
                    TSNotifier.hideProgress()
                    self.proceed()
                }
            } else {
                TSNotifier.hideProgress()
                self.proceed()
            }
        }
    }
    
    func prepareStoqi(completion : () -> Void) {
        TSNotifier.showProgressWithMessage("Preparing Stoqi..", onView: self.view)
        let group = GCDGroup()
        var result : AnyOperationResult = .Success
    
        if productsManager.products == nil {
            group.enter()
            self.productsManager.performLoadProducts {
                group.leave()
                if case .Success = result {
                    result = AnyOperationResult(result: $0)
                }
            }
        }
        if profileManager.profile == nil {
            group.enter()
            self.profileManager.performLoadProfile() {
                group.leave()
                if case .Success = result {
                    result = AnyOperationResult(result: $0)
                }
            }
        }
		if requestsManager.requests == nil {
			group.enter()
			self.requestsManager.performLoadRequests() {
				group.leave()
				if case .Success = result {
					result = AnyOperationResult(result: $0)
				}
			}
		}

		
        group.notify(GCDQueue.Main) {
            TSNotifier.hideProgress()
            switch result {
            case .Success:
                completion()
            case .Failure:
                let alert = UIAlertController(title: "Stoqi", message: "Failed to setup Stoqi for you. Please, try again later.", preferredStyle: .Alert)
                alert.addAction(UIAlertAction(title: "Try Again", style: .Default, handler: { _ in
                    self.prepareStoqi(completion)
                }))
                alert.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: { _ in

                }))
                self.presentViewController(alert, animated: true, completion: nil)
            }
        }
    }
}

// MARK: - Presenter 
extension LoginViewController : TSConfigurable {
    func configure(with dataSource: LoginViewModel) {
        self.tfLogin.text = dataSource.login.value
        self.tfPassword.text = dataSource.password.value
    }
}