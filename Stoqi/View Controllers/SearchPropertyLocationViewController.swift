import UIKit
import IQKeyboardManagerSwift

class SearchPropertyLocationViewController : StandaloneViewController {
    
    @IBOutlet weak private var aiSearchActivity: UIActivityIndicatorView!
    @IBOutlet weak private var tvSearchResults: UITableView!
    
    @IBOutlet weak private var tfSearch: UITextField!
    
    private let manager = try! Injector.inject(ProfileManager.self)
    
    private var viewModel = try! Injector.inject(SearchQueryViewModel.self) {
        didSet {
            self.configure(with: self.viewModel)
        }
    }
    private let emptyResultsVM  = try! Injector.inject(CommonEmptyResultsCellDataSource.self, with: "Sorry, we don't know such city.")
    
    var hasResults : Bool {
        return !(self.viewModel.results.isEmpty ?? true)
    }
    
    private(set) var selectedViewModel : SearchPropertyLocationViewModel?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tfSearch.addDoneOnKeyboardWithTarget(self, action: #selector(triggerSearch), titleText: "Search")
    }
    
    override func viewWillAppear(animated: Bool) {
        self.configure(with: self.viewModel)
        super.viewWillAppear(animated)
    }
}

// MARK: - Controller (ViewModel <=> DataManager)
private extension SearchPropertyLocationViewController {
    
    @objc func triggerSearch() {
        if let query = self.viewModel.query {
            self.search(withTerm: query)
        } else {
            print("\(self.dynamicType): Empty query.")
        }
        self.view.endEditing(true)
    }
    
    func search(withTerm term: String) {
        guard !self.viewModel.isSearching else {
            print("\(self.dynamicType): Search in progress.")
            return // busy
        }
        self.viewModel.isSearching = true
        self.manager.performSearchPropertyLocation(withTerm: term) { result in
            self.viewModel.isSearching = false
            if case .Success(let locations) = result {
                self.viewModel.results = locations.flatMap { try? Injector.inject(SearchPropertyLocationViewModel.self, with:SearchPropertyLocationInjectionParameter(location: $0, matchingTerm: term))
                }
                self.tvSearchResults.reloadData()
            }
        }
    }

}

// MARK: - Interactor (View => ViewModel)
private extension SearchPropertyLocationViewController {
    @IBAction func searchChanged(sender: AnyObject) {
        self.viewModel.query = self.tfSearch.text
    }
}

// MARK: - Presenter (ViewModel => View)
extension SearchPropertyLocationViewController : TSConfigurable {
    func configure(with dataSource: SearchQueryViewModel) {
        if dataSource.isSearching {
            self.aiSearchActivity.startAnimating()
        } else {
            self.aiSearchActivity.stopAnimating()
        }
    }
}

// MARK: - TableView Presenter (ViewModel => View)
extension SearchPropertyLocationViewController : UITableViewDelegate, UITableViewDataSource {

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.results.count ?? 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if self.hasResults {
            let cell = tableView.dequeueReusableCellOfType(SearchCityContentCell.self)
            cell.configure(with: self.viewModel.results[indexPath.row])
            return cell
        } else {
            let cell = tableView.dequeueReusableCellOfType(CommonEmptyResultsCell.self)
            cell.configure(with: self.emptyResultsVM)
            return cell
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.selectedViewModel = self.viewModel.results[indexPath.row]
    }
    
    func tableView(tableView: UITableView, willSelectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
        self.selectedViewModel = self.viewModel.results[indexPath.row]
        return indexPath
    }
    
}