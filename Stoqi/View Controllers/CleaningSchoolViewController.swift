class CleaningSchoolViewController: RefreshableBaseViewController {
    
    typealias `Self` = CleaningSchoolViewController
    
    private static let segOpenArticle = "segReadMore"
    
    private let manager = try! Injector.inject(ArticlesManager.self)
    
    private var viewModel : [ArticleItemViewModel] = [] {
        didSet {
            self.tvArticles?.reloadData()
        }
    }
    
    private var selectedArticle : ArticleItemViewModel? {
        didSet {
            self.performSegueWithIdentifier(Self.segOpenArticle, sender: self)
        }
    }
    
    @IBAction func unwindArticle(segue : UIStoryboardSegue) { }
    
    @IBOutlet weak private var tvArticles: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tvArticles.rowHeight = UITableViewAutomaticDimension
        self.tvArticles.estimatedRowHeight = ArticleCell.height
        if self.manager.articles == nil {
            self.loadArticles()
        }
    }
    
    func refresh(sender: UIRefreshControl) {
        self.loadArticles(true)
    }
    
    private func loadArticles(refreshing : Bool = false) {
        if !refreshing {
            TSNotifier.showProgressWithMessage("Loading..", onView: self.view)
        }
        self.manager.performLoadArticles {
            if refreshing {
                self.refreshing = false
                if case .Success = $0 {
                    self.updateRefresher()
                }
            } else {
                TSNotifier.hideProgress()
            }
            if case .Success(let articles) = $0 {
                self.viewModel = articles.flatMap {try? Injector.inject(ArticleItemViewModel.self, with: $0)}
            }
        }
    }
    
    // MARK: - Refresher Implementation
    override func refresh() {
        self.loadArticles(true)
    }
    
    override var refresherTableView: UITableView? {
        return self.tvArticles
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        guard let id = segue.identifier where id == Self.segOpenArticle else {
            return
        }
        
        guard let controller = segue.destinationViewController as? ReadArticleViewController else {
            return
        }
        
        controller.setArticle(self.selectedArticle!.article)
    }
}

// MARK: - TableView Presenter (ViewModel => View)
extension CleaningSchoolViewController : UITableViewDataSource {
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellOfType(ArticleCell.self)
        let articleViewModel = self.viewModel[indexPath.row]
        cell.configure(with: articleViewModel)
        cell.style(with: articleViewModel)
        cell.delegate = { self.selectedArticle = articleViewModel }
        return cell
    }
}