import MTBBarcodeScanner

class ScannerViewController: BaseViewController {

    private static let segScanned = "segScanned"
    
    private let manager = try! Injector.inject(ProductsManager.self)
    
    private var scanner : MTBBarcodeScanner!
    
    @IBOutlet private weak var vCamera: UIView!
    
    
    private(set) var product : Product?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scanner = MTBBarcodeScanner(previewView: vCamera)
        scanner.camera = .Back
        startScanner()
    }

    private func requestScanner() {
        MTBBarcodeScanner.requestCameraPermissionWithSuccess({
            if $0 {
                self.startScanner()
            }
        })
    }
    
    private func startScanner() {
        guard MTBBarcodeScanner.cameraIsPresent() else {
            TSNotifier.notify("You haven't got a camera :(", onView: self.view)
            return
        }
        guard !MTBBarcodeScanner.scanningIsProhibited() else {
            requestScanner()
            return
        }
        
        scanner.startScanningWithResultBlock({
            guard let code = $0.first as? String else {
                return
            }
            self.scanner.stopScanning()
            TSNotifier.showProgressOnView(self.view)
            self.manager.performFindProduct(code, callback: {
                TSNotifier.hideProgress()
                if case .Success(let product) = $0 {
                    self.product = product
                    self.performSegueWithIdentifier(self.dynamicType.segScanned, sender: self)
                } else {
                    TSNotifier.notify("Uknown product", onView: self.view)
                    self.startScanner()
                }
            })
            }, error: nil)
    }
}
