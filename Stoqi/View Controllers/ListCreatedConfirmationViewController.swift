class ListCreatedConfirmationViewController : BaseViewController {
    
    private static let segOpenList = "segOpenList"
    
    private let manager = try! Injector.inject(RequestsManager.self)
    
    
    @IBAction func openList(sender: UIButton) {
        guard self.manager.requests != nil else {
            self.loadRequests()
            return
        }
        self.performSegueWithIdentifier(self.dynamicType.segOpenList, sender: self)
    }
    
    
    private func loadRequests() {
        TSNotifier.showProgressWithMessage("Preparing Stoqi..", onView: self.view)
        self.manager.performLoadRequests {
            TSNotifier.hideProgress()
            switch $0 {
            case .Success(let requests):
                Storage.temp[kStorageRequestListValue] = requests.first
                self.performSegueWithIdentifier(self.dynamicType.segOpenList, sender: self)
            case let .Failure(error):
				
				if error == .NetworkError {
					TSNotifier.notify("kNoInternetConnection".localized, withAppearance: [kTSNotificationAppearanceTextColor : UIColor.redColor()], onView: self.view)
				}
				
                let alert = UIAlertController(title: "Stoqi", message: "Failed to setup Stoqi for you. Please, try again later.", preferredStyle: .Alert)
                alert.addAction(UIAlertAction(title: "Try Again", style: .Default, handler: { _ in
                    self.loadRequests()
                }))
                alert.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: { _ in
                    
                }))
                self.presentViewController(alert, animated: true, completion: nil)
            }
        }
    }
    
}