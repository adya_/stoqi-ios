class PurchaseViewController : BaseViewController {
    
    typealias `Self` = PurchaseViewController
    private static let segInitialClose = "segClose"
    private static let segSubsequentClose = "segCancel"
    private static let segInitialBuy = "segClose"
    private static let segSubsequentBuy = "segBuy"
	private static let segToCards = "segToCards"
	private static let segToAddress = "segToAddress"
    
    private var manager = try! Injector.inject(RequestsManager.self)
	private let profileManager = try! Injector.inject(ProfileManager.self)
    
    var mode : RequestListViewControllerMode!
    var request : RequestList!
    private var viewModel : PurchaseViewModel!
    
    @IBOutlet weak var bClose: UIButton!
    @IBOutlet weak var lTotal: UILabel!
    @IBOutlet weak var lSavings: UILabel!
	@IBOutlet weak var bBuy: UIButton!

    private var segClose : String!
    private var segBuy : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let isInitial = self.mode! == .Initial // weird
        self.segClose = isInitial ? Self.segInitialClose : Self.segSubsequentClose
        self.segBuy = isInitial ? Self.segInitialBuy : Self.segSubsequentBuy
        self.bClose.hidden = !isInitial
        self.viewModel = try! Injector.inject(PurchaseViewModel.self, with: self.request)
        self.configure(with: self.viewModel)
    }
	
	private func openCards()
	{
		performSegueWithIdentifier(Self.segToCards, sender: self)
	}
	
	private func openProfile()
	{
		performSegueWithIdentifier(Self.segToAddress, sender: self)
	}
    
    @IBAction func buyAction(sender: AnyObject) {
		
		let noCards = profileManager.profile?.cards?.isEmpty ?? true
		
		if noCards || profileManager.profile?.address == nil {
			
			let alert = UIAlertController(title: "Stoqi", message: noCards ? "You haven't added any cards yet" : "You haven't set delivery address yet", preferredStyle: .Alert)
			alert.addAction(UIAlertAction(title: noCards ? "Add Card" : "Add Address", style: .Default, handler: { _ in
				if noCards
				{
					self.openCards()
				}
				else
				{
					self.openProfile()
				}
				
				Storage.temp[kStorageBackToPurchase] = true
			}))
			alert.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: { _ in
				
			}))
			self.presentViewController(alert, animated: true, completion: nil)
			
			return
		}
		
		
		
        TSNotifier.showProgressWithMessage("Buying..", onView: self.view)
        self.manager.performProcessRequest(self.request) {
            TSNotifier.hideProgress()
            if case .Success = $0 {
                 self.performSegueWithIdentifier(self.segBuy, sender: self)
            }
			else if case let .Failure(error) = $0 {
				self.showError(error, errorMessage: "Error".localized)
			}
        }
    }
    @IBAction func closeAction(sender: UIButton) {
        self.performSegueWithIdentifier(self.segClose, sender: self)
    }
}

extension PurchaseViewController : TSConfigurable {
    func configure(with dataSource : PurchaseViewModel) {
        self.lSavings.text = "R$ \(String(format:"%.2f", dataSource.saved))"
        self.lTotal.text = "R$ \(String(format:"%.2f", dataSource.total))"
		self.bBuy.setTitle(dataSource.moreThen7days ? "Authorize purchase" : "Buy", forState: UIControlState.Normal)
    }
}