import UIKit

class PropertyLocationViewController : StandaloneViewController, QuestionEditor {
    
    @IBOutlet weak private var vHolder: UIView!
    @IBOutlet weak private var lCity: UILabel!
    @IBOutlet weak private var lRegion: UILabel!
    
    var delegate: QuestionEditorDelegate?
    var question : QuestionViewModel {
        get {
            return viewModel
        }
        set {
            viewModel = newValue as! PropertyLocationQuestionViewModel
        }
    }
    
    private var viewModel : PropertyLocationQuestionViewModel! {
        didSet {
            guard isViewLoaded() else {
                return
            }
            self.configure(with: viewModel)
            delegate?.questionEditor(self, didEditQuestion: viewModel)
        }
    }
    
    override func viewWillAppear(animated : Bool) {
        super.viewWillAppear(animated)
        configure(with: viewModel)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.vHolder.circle = true // reset circle to recalculate cornerRadius since vHolder will be dynamically resized based on constraints.
    }

    @IBAction func unwindSelectedCity(segue : UIStoryboardSegue) {
        if let controller = segue.sourceViewController as? SearchPropertyLocationViewController,
            selectedViewModel = controller.selectedViewModel {
            self.viewModel = try! Injector.inject(PropertyLocationQuestionViewModel.self, with: selectedViewModel.location)
        }
    }
}

// MARK: - Presenter (ViewModel => View)
extension PropertyLocationViewController : TSConfigurable {
    func configure(with dataSource: PropertyLocationQuestionViewModel) {
        if let city = dataSource.city,
            region = dataSource.region {
            self.lCity.text = city
            self.lRegion.text = region
        }
    }
}