import UIKit

class PropertyTypeViewController : StandaloneViewController, QuestionEditor {
    
    @IBOutlet weak private var bApartment: UIButton!
    @IBOutlet weak private var bHouse: UIButton!
    @IBOutlet weak private var bCommercial: UIButton!

    var delegate: QuestionEditorDelegate?
    var question : QuestionViewModel {
        get {
            return viewModel
        }
        set {
            viewModel = newValue as! PropertyTypeQuestionViewModel
        }
    }
    
    private var viewModel : PropertyTypeQuestionViewModel! {
        didSet {
            guard isViewLoaded() else {
                return
            }
            self.configure(with: viewModel)
            delegate?.questionEditor(self, didEditQuestion: viewModel)
        }
    }
    
    override func viewWillAppear(animated : Bool) {
        super.viewWillAppear(animated)
        configure(with: viewModel)
    }
}


// MARK: - Interactor (View => ViewModel)
private extension PropertyTypeViewController {
    @IBAction func optionSelected(sender: UIButton) {
        
        switch sender {
        case self.bApartment: self.viewModel.type = .Apartment
        case self.bHouse: self.viewModel.type = .House
        case self.bCommercial: self.viewModel.type = .CommercialRoom
        default: break
        }
    }
}

extension PropertyTypeViewController : TSConfigurable {
    func configure(with dataSource: PropertyTypeQuestionViewModel) {
        self.bHouse.selected = dataSource.type == .House
        self.bApartment.selected = dataSource.type == .Apartment
        self.bCommercial.selected = dataSource.type == .CommercialRoom
    }
}