
class ReadArticleViewController : BaseViewController {
    
    @IBOutlet weak var svContent: UIScrollView!
    @IBOutlet weak var ivArticleImage: UIImageView!
    @IBOutlet weak var tvContent: UITextView!
    
    private var viewModel : ArticleViewModel!
    
    func setArticle(article : Article) {
        self.viewModel = try! Injector.inject(ArticleViewModel.self, with: article)
    }
    
    override func viewWillAppear(animated : Bool) {
        super.viewWillAppear(animated)
        self.configure(with: self.viewModel)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.adjustScrollContent()
    }
}

extension ReadArticleViewController : TSConfigurable {
   
    func configure(with dataSource: ArticleViewModel) {
        self.ivArticleImage.image = dataSource.image
        self.tvContent.attributedText = self.buildArticleContent(dataSource.title, paragraphs: dataSource.paragraphs)
        self.view.setNeedsLayout()
    }
    
    private func adjustScrollContent() {
        let size = self.tvContent.sizeThatFits(CGSize(width: self.view.bounds.width, height: CGFloat.infinity))
        self.tvContent.frame.size = CGSize(width: self.tvContent.frame.width, height: size.height)
        self.svContent.contentSize = CGSize(width: size.width, height: self.ivArticleImage.frame.height + size.height)

    }
    
    private func buildArticleContent(title : String, paragraphs : [(String, String)]) -> NSAttributedString {
        let attrString = NSMutableAttributedString()
        attrString.appendAttributedString(self.buildArticleTitle("\(title)\n\n"))
        paragraphs.forEach {
            attrString.appendAttributedString(self.buildParagraphTitle("\($0.0)\n\n"))
            attrString.appendAttributedString(NSAttributedString(string: "\($0.1)\n\n"))
        }
        
        // Set Justify alignment
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.Justified
       
        attrString.addAttributes([
            NSParagraphStyleAttributeName: paragraphStyle,
            NSBaselineOffsetAttributeName: 0
            ], range: attrString.string.nsrange)
        return NSAttributedString(attributedString: attrString)
    }
    
    private func buildParagraphTitle(title : String) -> NSAttributedString {
        return NSAttributedString(string: title, attributes: [NSFontAttributeName : UIFont.boldSystemFontOfSize(14),
            NSForegroundColorAttributeName : StoqiPallete.darkGrayTextColor])
    }
    private func buildArticleTitle(title : String) -> NSAttributedString {
        return NSAttributedString(string: title, attributes: [NSFontAttributeName : UIFont.boldSystemFontOfSize(16),
            NSForegroundColorAttributeName : StoqiPallete.mainColor])
    }
}