protocol CardEditorDelegate {
    func editor(editor : CardEditor, didEdit viewModel: CardViewModel)
    func editorDidCancel(editor : CardEditor)
}

protocol CardEditor {
    var viewModel : CardViewModel! {get}
    var delegate : CardEditorDelegate? {get set}
    func setCard(card : Card)
    func reset()
}

class AddCardViewController: StandaloneViewController, CardEditor {
    @IBOutlet weak private var tvNumber: UITextField!
    @IBOutlet weak private var tvName: UITextField!
    @IBOutlet weak private var tvCode: UITextField!
    @IBOutlet weak private var bMonth: TSOptionButton!
    @IBOutlet weak private var bYear: TSOptionButton!
    
    private var previousTextFieldContent : String?
    private var previousSelection : UITextRange?
    
    var viewModel : CardViewModel! {
        didSet {
            if let viewModel = viewModel where isViewLoaded() {
                configure(with: viewModel)
            }
        }
    }
    
    var delegate: CardEditorDelegate?
    
    func setCard(card: Card) {
        viewModel = try! Injector.inject(CardViewModel.self, with: card)
    }
    
    func reset() {
        viewModel = try! Injector.inject(CardViewModel.self)
    }
    
    private var monthPickerDataSource = try! Injector.inject(PickerDataSource.self, with: PickerInjectionParameter.MonthsPicker)
    
    private var yearPickerDataSource = try! Injector.inject(PickerDataSource.self, with: PickerInjectionParameter.YearsPicker)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if viewModel == nil {
            viewModel = try! Injector.inject(CardViewModel.self)
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        if let viewModel = viewModel {
            configure(with: viewModel)
        }
        resetErrors()
    }

}

// MARK: Card input formatting
extension AddCardViewController : UITextFieldDelegate {
    @IBAction func reformatAsCardNumber(textField: UITextField) {
        var targetCursorPosition = 0
        if let startPosition = textField.selectedTextRange?.start {
            targetCursorPosition = textField.offsetFromPosition(textField.beginningOfDocument, toPosition: startPosition)
        }
        
        var cardNumberWithoutSpaces = ""
        if let text = textField.text {
            cardNumberWithoutSpaces = self.removeNonDigits(text, andPreserveCursorPosition: &targetCursorPosition)
        }
        
        if cardNumberWithoutSpaces.characters.count > 19 {
            textField.text = previousTextFieldContent
            textField.selectedTextRange = previousSelection
            return
        }
        
        let cardNumberWithSpaces = self.insertSpacesEveryFourDigitsIntoString(cardNumberWithoutSpaces, andPreserveCursorPosition: &targetCursorPosition)
        textField.text = cardNumberWithSpaces
        
        if let targetPosition = textField.positionFromPosition(textField.beginningOfDocument, offset: targetCursorPosition) {
            textField.selectedTextRange = textField.textRangeFromPosition(targetPosition, toPosition: targetPosition)
        }
    }
    
    func removeNonDigits(string: String, inout andPreserveCursorPosition cursorPosition: Int) -> String {
        var digitsOnlyString = ""
        let originalCursorPosition = cursorPosition
        
        for i in 0..<string.characters.count {
            let characterToAdd = string[string.startIndex.advancedBy(i)]
            if characterToAdd >= "0" && characterToAdd <= "9" {
                digitsOnlyString.append(characterToAdd)
            }
            else if i < originalCursorPosition {
                cursorPosition -= 1
            }
        }
        
        return digitsOnlyString
    }
    
    // TODO: Implement advanced card formatting (for acceptable card issuers)
    func insertSpacesEveryFourDigitsIntoString(string: String, inout andPreserveCursorPosition cursorPosition: Int) -> String {
        var stringWithAddedSpaces = ""
        let cursorPositionInSpacelessString = cursorPosition
        
        for i in 0..<string.characters.count {
            if i > 0 && (i % 4) == 0 {
                stringWithAddedSpaces += " "
                if i < cursorPositionInSpacelessString {
                    cursorPosition += 1
                }
            }
            let characterToAdd = string[string.startIndex.advancedBy(i)]
            stringWithAddedSpaces.append(characterToAdd)
        }
        
        return stringWithAddedSpaces
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        previousTextFieldContent = textField.text;
        previousSelection = textField.selectedTextRange;
        return true
    }
}

extension AddCardViewController {
    
    @IBAction func optionTriggeredAction(sender: TSOptionButton) {
        view.endEditing(true)
        switch sender {
        case bMonth: openPicker(monthPickerDataSource, trigger: sender) { (index, item) in
            self.monthPickerDataSource.initialValue = item
            self.viewModel.month.value = (item as! MonthPickerItemViewModel).month
        }
        case bYear: openPicker(yearPickerDataSource, trigger: sender) { (index, item) in
            self.yearPickerDataSource.initialValue = item
            self.viewModel.year.value = (item as! YearPickerItemViewModel).year
            
        }
        default: break
        }
        setButton(sender, valid: true)
    }

    @IBAction func changeAction(sender: AnyObject) {
        view.endEditing(true)
        if viewModel.isValid {
            delegate?.editor(self, didEdit: viewModel)
        } else {
            highlightErrors()
            TSNotifier.notify("Please, enter valid values",
                              withAppearance: [kTSNotificationAppearancePositionYOffset : 16],
                              onView: presentingViewController?.view ?? view)
        }
    }
    
    @IBAction func cancelAction(sender: UIButton) {
        view.endEditing(true)
        delegate?.editorDidCancel(self)
    }
    
    @IBAction func editedAction(sender: UITextField) {
        switch sender {
        case tvName: viewModel.owner.value = sender.text
        case tvNumber:
            if let text = sender.text {
                let card = text.stringByReplacingOccurrencesOfString(
                    "[^\\d]", withString: "", options: .RegularExpressionSearch,
                    range: text.startIndex..<text.endIndex)
                viewModel.number.value = card
            } else {
                viewModel.number.value = nil
            }
        case tvCode: viewModel.code.value = sender.text
        default: break
        }
        setView(sender, valid: true)
    }
}

extension AddCardViewController : TSConfigurable {
    func configure(with dataSource: CardViewModel) {
        tvNumber.text = dataSource.number.value
        reformatAsCardNumber(tvNumber)
        tvName.text = dataSource.owner.value
        tvCode.text = dataSource.code.value
        bMonth.setTitle(dataSource.month.value.map{String(format:"%02d", $0)}, forState: .Normal)
        bYear.setTitle(dataSource.year.value.map{String(format:"%02d", $0)}, forState: .Normal)
    }
    
    func highlightErrors() {
        [(tvNumber, viewModel?.number.isValid),
         (tvCode,   viewModel?.code.isValid),
         (tvName,   viewModel?.owner.isValid)].forEach {
            setView($0.0, valid: $0.1 ?? true)
        }
        [(bMonth, viewModel?.month.isValid),
         (bYear,  viewModel?.year.isValid)].forEach {
            setButton($0.0, valid: $0.1 ?? true)
        }
    }
    
    func resetErrors() {
        [tvNumber, tvCode, tvName].forEach {
            setView($0, valid: true)
        }
        [bMonth, bYear].forEach {
            setButton($0, valid: true)
        }
    }
    
    func setButton(button : UIButton, valid : Bool) {
        setView(button, valid: valid, validColor: StoqiPallete.mainColor)
    }
    
    
}
