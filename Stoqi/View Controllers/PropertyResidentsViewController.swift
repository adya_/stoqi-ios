class PropertyResidentsViewController : StandaloneViewController, QuestionEditor {
    
    @IBOutlet weak private var lAdults: UILabel!
    @IBOutlet weak private var lChildren: UILabel!
    @IBOutlet weak private var lPets: UILabel!
    @IBOutlet weak private var sAdults: UIStepper!
    @IBOutlet weak private var sChildren: UIStepper!
    @IBOutlet weak private var sPets: UIStepper!
    @IBOutlet weak private var lAdultsLabel: UILabel!
    @IBOutlet weak private var lChildrenLabel: UILabel!
    @IBOutlet weak private var lPetsLabel: UILabel!
    
    var delegate: QuestionEditorDelegate?
    var question : QuestionViewModel {
        get {
            return viewModel
        }
        set {
            viewModel = newValue as! PropertyResidentsQuestionViewModel
        }
    }
    
    private var viewModel : PropertyResidentsQuestionViewModel! {
        didSet {
            guard isViewLoaded() else {
                return
            }
            self.configure(with: viewModel)
            delegate?.questionEditor(self, didEditQuestion: viewModel)
        }
    }
    
    override func viewWillAppear(animated : Bool) {
        super.viewWillAppear(animated)
        configure(with: viewModel)
    }
}

// MARK: - Presenter (ViewModel => View)
extension PropertyResidentsViewController : TSConfigurable {
    
    func configure(with dataSource: PropertyResidentsQuestionViewModel) {
        self.lAdults.text = "\(dataSource.adults.value ?? 0)"
        self.lChildren.text = "\(dataSource.children.value ?? 0)"
        self.lPets.text = "\(dataSource.pets.value ?? 0)"
        self.sAdults.value = Double(dataSource.adults.value ?? 0)
        self.sChildren.value = Double(dataSource.children.value ?? 0)
        self.sPets.value = Double(dataSource.pets.value ?? 0)
        
        self.lAdultsLabel.text = dataSource.adults.value ?? 0 == 1 ? "Adult" : "Adults"
        self.lChildrenLabel.text = dataSource.children.value ?? 0 == 1 ? "Child" : "Children"
        self.lPetsLabel.text = dataSource.pets.value ?? 0 == 1 ? "Dog / Cat" : "Dogs / Cats"
    }
}

// MARK: - Interactor (View => ViewModel)
private extension PropertyResidentsViewController {
    
    @IBAction func stepperValueChanged(sender: UIStepper) {
        let value = Int(sender.value)
        switch sender {
        case self.sAdults: self.viewModel.adults.value = value
        case self.sChildren: self.viewModel.children.value = value
        case self.sPets: self.viewModel.pets.value = value
        default: break
        }
    }
}