import UIKit

class HelpViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    private let helpOptionsDS : [HelpOptionCellDataSource] = [SimpleHelpOptionDataSource(title: "How the Stoqi works?", icon: "help_about"),
                                                              SimpleHelpOptionDataSource(title: "When I receive my replacement?", icon: "help_receive_replacement"),
                                                              SimpleHelpOptionDataSource(title: "How can I request a purchase?", icon: "help_about"),
                                                              SimpleHelpOptionDataSource(title: "Send an email to Stoqi", icon: "help_mail_us", canOpen: false),
                                                              SimpleHelpOptionDataSource(title: "Talk with us", icon: "help_talk_to_us", canOpen: false)]
    
    @IBOutlet weak private var tvOptions: UITableView!
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.helpOptionsDS.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return HelpOptionCell.height
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellOfType(HelpOptionCell.self)
        cell.configure(with: helpOptionsDS[indexPath.row])
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath
            , animated: true)
    }
}

private struct SimpleHelpOptionDataSource : HelpOptionCellDataSource {
    var title: String
    var icon: UIImage
    var canOpen: Bool
    
    init(title: String, icon : String, canOpen : Bool = true) {
        self.title = title
        self.icon = UIImage(named:icon)!
        self.canOpen = canOpen
    }
}
