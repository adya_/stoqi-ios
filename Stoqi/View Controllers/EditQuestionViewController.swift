class EditQuestionViewController : EmbedingViewController, QuestionEditor {
    private static let segClose = "segClose"
    
    @IBOutlet weak private var lQuestion : UILabel!
    @IBOutlet weak private var bChange: UIButton!
   
    private var editor : QuestionEditor!
    
    var delegate: QuestionEditorDelegate?
    var question: QuestionViewModel {
        get {
            return editor.question
        }
        set {
            if newValue is PropertyLocationQuestionViewModel {
                editor = createQuestionViewController(PropertyLocationViewController.self)
            } else if newValue is PropertyAreaQuestionViewModel {
                editor = createQuestionViewController(PropertyAreaViewController.self)
            } else if newValue is PropertyTypeQuestionViewModel {
                editor = createQuestionViewController(PropertyTypeViewController.self)
            } else if newValue is PropertyResidentsQuestionViewModel {
                editor = createQuestionViewController(PropertyResidentsViewController.self)
            } else if newValue is ProductsPriorityQuestionViewModel {
                editor = createQuestionViewController(ProductsPriorityViewController.self)
            } else {
                editor = nil
                print("No suitable ViewController found for question \(newValue.dynamicType)")
            }
            editor.question = newValue
            editor.delegate = self
        }
    }
    
    private func createQuestionViewController<T where T : StandaloneViewController, T : QuestionEditor>(controllerType : T.Type) -> T? {
        
        if controllerType == PropertyLocationViewController.self {
            return UIStoryboard(name: PropertyLocationViewController.identifier, bundle: nil).instantiateInitialViewController() as? T
        } else {
            return controllerType.init()
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        let questionPage = editor as! UIViewController
        embed(questionPage)
        configure(with: question)
    }
    
}

extension EditQuestionViewController : QuestionEditorDelegate {
    func questionEditor(editor: QuestionEditor, didEditQuestion question: QuestionViewModel) {
        configure(with: question)
        delegate?.questionEditor(self, didEditQuestion: question)
    }
    
    func saveQuestion() {
        performSegueWithIdentifier(self.dynamicType.segClose, sender: self)
    }
}

// MARK: - Controller
private extension EditQuestionViewController {
    @IBAction func changeAction(sender: UIButton) {
        saveQuestion()
    }
}

// MARK: - Presenter
extension EditQuestionViewController : TSConfigurable {
    func configure(with dataSource: QuestionViewModel) {
        lQuestion.text = dataSource.question
        bChange.enabled = dataSource.isValid
    }
}
