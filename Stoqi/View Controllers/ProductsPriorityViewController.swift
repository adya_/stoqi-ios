import UIKit

class ProductsPriorityViewController : StandaloneViewController, QuestionEditor {
    
    @IBOutlet weak private var bPrice: UIButton!
    @IBOutlet weak private var bBrand: UIButton!
    @IBOutlet weak private var bMixed: UIButton!
    
    var delegate: QuestionEditorDelegate?
    var question : QuestionViewModel {
        get {
            return viewModel
        }
        set {
            viewModel = newValue as! ProductsPriorityQuestionViewModel
        }
    }
    
    private var viewModel : ProductsPriorityQuestionViewModel! {
        didSet {
            guard isViewLoaded() else {
                return
            }
            self.configure(with: viewModel)
            delegate?.questionEditor(self, didEditQuestion: viewModel)
        }
    }
    
    override func viewWillAppear(animated : Bool) {
        super.viewWillAppear(animated)
        configure(with: viewModel)
    }
}

// MARK: - Interactor
extension ProductsPriorityViewController {
    @IBAction func optionSelected(sender: UIButton) {
        
        switch sender {
        case self.bPrice: self.viewModel.priority = .Price
        case self.bBrand: self.viewModel.priority = .Brand
        case self.bMixed: self.viewModel.priority = .Both
        default: break
        }
        
    }
}

// MARK: - Presenter
extension ProductsPriorityViewController {
    func configure(with dataSource: ProductsPriorityQuestionViewModel) {
        self.bPrice.selected = dataSource.priority == .Price
        self.bBrand.selected = dataSource.priority == .Brand
        self.bMixed.selected = dataSource.priority == .Both
    }
}