protocol StockProductEditorDelegate {
    func editor(editor : StockProductEditor, didEdit viewModel : StockProductViewModel)
    func editorDidCancel(editor : StockProductEditor)
}

protocol StockProductEditor {
    var viewModel : StockProductViewModel! {get}
    var delegate : StockProductEditorDelegate? {get set}
    func setProduct(product : StockProduct)
}

class EditStockProductViewController: StandaloneViewController, StockProductEditor {
    var delegate: StockProductEditorDelegate?
    var viewModel: StockProductViewModel! {
        didSet {
            if let viewModel = viewModel where isViewLoaded() {
                configure(with: viewModel)
            }
        }
    }
    
    @IBOutlet private weak var sUnits: UIStepper!
    @IBOutlet private weak var lUnits: UILabel!
    @IBOutlet private weak var lUnitsLabel: UILabel!
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        if let viewModel = viewModel {
            configure(with: viewModel)
        }
    }
    
    func setProduct(product: StockProduct) {
        viewModel = try! Injector.inject(StockProductViewModel.self, with: product)
    }
}

extension EditStockProductViewController : TSConfigurable {
    func configure(with dataSource: StockProductViewModel) {
        let left = dataSource.left % 0.5 == 0 ? String(format: "%.1f", dataSource.left) : String(format: "%.2f",dataSource.left)
        lUnits.text = left
        lUnitsLabel.text = dataSource.left == 1.0 ? "Unit" : "Units"
        sUnits.value = Double(dataSource.left)
        sUnits.maximumValue = Double(dataSource.total)
    }
}


extension EditStockProductViewController {
    
    @IBAction func unitsChangedAction(sender: UIStepper) {
        viewModel.left = Float(sender.value)
    }
    
    @IBAction func changeAction(sender: UIButton) {
        delegate?.editor(self, didEdit: viewModel)
    }
    
    @IBAction func cancelAction(sender: UIButton) {
        delegate?.editorDidCancel(self)
    }
}