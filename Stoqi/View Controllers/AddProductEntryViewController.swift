let kStorageProductEntryCategory = "kStorageProductEntryCategory"
let kStorageScannerTutorial = "kStorageScannerTutorial"

class AddProductEntryViewController : EmbedingViewController, ProductEntryEditorDelegate, UIScrollViewDelegate {

    private static let segAddProduct = "segAddProduct"
    private static let segTutorial = "segTutorial"
   
    private let manager = try! Injector.inject(ProductsManager.self)
    
    @IBOutlet private weak var bAdd: UIButton!
    @IBOutlet private weak var bScanner: UIButton!
    
    @IBInspectable var useAccentTheme : Bool = false
    private var editor : ProductEntryEditor!
    
    var product : ProductEntry? {
        guard let entry = editor?.viewModel?.productEntry else {
            return nil
        }
        let product = entry.product
        return manager.products?.filter {
            $0.brand == product.brand &&
                $0.kind == product.kind &&
                $0.volume == product.volume
            }.first.flatMap { ProductEntry(product: $0, units: entry.units) }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let controller = embed(ProductEntryEditorViewController.self)
        controller.delegate = self
        controller.useAccentTheme = useAccentTheme
        editor = controller
        let showTutorial = Storage.local[kStorageScannerTutorial] as? Bool ?? true
        if showTutorial {
            performSegueWithIdentifier(self.dynamicType.segTutorial, sender: self)
            Storage.local[kStorageScannerTutorial] = false
        }
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        guard let category = Storage.temp.popObjectForKey(kStorageProductEntryCategory) as? Category else {
            print("\(self.dynamicType): Category was not set.")
            return
        }
        editor.setCategory(category)
        let categoryName = category.name.capitalizedString
        bAdd.setTitle("Add in \(categoryName)", forState: .Normal)
    }
    
    func editor(editor: ProductEntryEditor, didEditProductEntry product: ProductViewModel) {
        bAdd.enabled = product.isValid
    }
    
    // Locks top-edge bouncing
    func scrollViewDidScroll(scrollView: UIScrollView) {
        if scrollView.contentOffset.y <= 0 {
            scrollView.contentOffset = CGPointZero
        }
    }
}

extension AddProductEntryViewController : UIPopoverPresentationControllerDelegate {
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        guard let controller = segue.destinationViewController.popoverPresentationController else {
            return
        }
        controller.delegate = self
        controller.sourceRect = bScanner.bounds
        controller.sourceView = bScanner
        segue.destinationViewController.modalInPopover = true
    }
    
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return .None
    }
}

extension AddProductEntryViewController {
    @IBAction func addAction(sender: UIButton) {
        addProduct()
    }
    
    @IBAction func closeScannerTutorial(segue : UIStoryboardSegue) {}
    @IBAction func unwindScannerCancel(segue : UIStoryboardSegue) {}
    @IBAction func unwindScannerScanned(segue : UIStoryboardSegue) {
        guard let scanner = segue.sourceViewController as? ScannerViewController, product = scanner.product else {
            return
        }
        let entry = ProductEntry(product: product, units: 1)
        editor.setProduct(entry)
    }
}

extension AddProductEntryViewController {
    func addProduct() {
        guard var product = self.editor.viewModel?.productEntry else {
            TSNotifier.notify("Please, fill in all product details", withAppearance: [kTSNotificationAppearanceTextColor : UIColor.redColor()])
            return
        }
        
        guard let registeredProduct = self.manager.products?.filter({$0 === product.product}).first else {
            print("\(self.dynamicType): No matching product found in database.")
            return
        }
        product.product = registeredProduct
        performSegueWithIdentifier(self.dynamicType.segAddProduct, sender: self)
    }

}
