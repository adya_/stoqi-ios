
class MainViewController: BaseViewController {
    
    private static let segLogout = "segLogout"
	private static let segToYourList = "segToYourList"
    
    private let manager = try! Injector.inject(AuthorizationManager.self)
    private let profileManager = try! Injector.inject(ProfileManager.self)
	private let requestsManager = try! Injector.inject(RequestsManager.self)
    
    @IBOutlet weak private var lReplacementDate: UILabel!
    @IBOutlet weak private var lMonthlyAverage: UILabel!
    @IBOutlet weak private var lTotalSaved: UILabel!
    @IBOutlet weak private var lWelcome: UILabel!
	@IBOutlet weak private var lStoqi: UILabel!
 
    private var viewModel : HomeViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let profile = profileManager.profile, requests = requestsManager.requests else {
            print("FATAL ERROR: User data wasn't loaded!.")
            return
        }
		self.viewModel = try! Injector.inject(HomeViewModel.self, with: HomeInjectionParameter(profile:  profile, requests: requests))
        self.configure(with: self.viewModel)
    }
	
	func openYourList()
	{
		self.performSegueWithIdentifier(self.dynamicType.segToYourList, sender: self)
	}
}

// MARK: - Presenter
extension MainViewController : TSConfigurable {
    func configure(with dataSource: HomeViewModel) {
        self.lReplacementDate.text = "\(dataSource.minDeliveryDate.toString(withFormat: "dd MMM")) - \(dataSource.maxDeliveryDate.toString(withFormat: "dd MMM"))"
        self.lTotalSaved.text = "R$ \(String(format:"%.2f", dataSource.totalSaved))"
        self.lMonthlyAverage.text = "R$ \(String(format:"%.2f", dataSource.monthlySavings))"
        self.lWelcome.text = "Hello, \(dataSource.userName)"
		self.lStoqi.text = dataSource.canRefill ? "Refill my" : "Check my"
    }
}

// MARK: - Controller 
private extension MainViewController {
    @IBAction func unwindProfile(segue : UIStoryboardSegue) {
        
    }
	
    @IBAction func logoutAction(sender: UIButton) {
        manager.performLogout()
        self.performSegueWithIdentifier(self.dynamicType.segLogout, sender: self)
    }

}