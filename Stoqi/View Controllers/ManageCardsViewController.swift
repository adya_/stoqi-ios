let kStorageBackToPurchase = "kStorageBackToPurchase"

class ManageCardsViewController: BaseViewController, UIPopoverPresentationControllerDelegate {
    
    private enum Segues : String {
        case Back = "segBack"
        case PrimaryCard = "segPrimaryCard"
		case Purchase = "segToPurchase"
	}
    
    @IBOutlet weak private var tvCards: UITableView!
    
    private var viewModel : [CardViewModel]!
    private let emptyViewModel = try! Injector.inject(CommonEmptyResultsCellDataSource.self, with: "You haven't added any cards yet.")
    
    private var editor : AddCardViewController!
    
    private let manager = try! Injector.inject(ProfileManager.self)
    
    var cards : [Card]? {
        return viewModel?.flatMap{$0.card}
    }
    
    private(set) var selectedCard : Card?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        editor = AddCardViewController()
        editor.modalPresentationStyle = .Popover
        editor.modalInPopover = true
        editor.preferredContentSize = CGSize(width: 316, height: 178)
        editor.delegate = self
        
        viewModel = manager.profile?.cards?.flatMap {try! Injector.inject(CardViewModel.self, with: $0)} ?? []
        
        tvCards.rowHeight = UITableViewAutomaticDimension
        tvCards.estimatedRowHeight = ProfilePayMethodCell.height
    }
    
    func saveProfile() {
        if let cards = cards where !isSaved {
            TSNotifier.showProgressWithMessage("Saving Profile..", onView: view)
            TSNotifier.hideProgress()
            manager.performSaveProfile(Profile(cards: cards)) {
                if case .Success = $0 {
                    TSNotifier.notify("Profile saved!")
				} else if case let .Failure(error) = $0 {
					self.showError(error, errorMessage: "Failed to save profile.".localized)
				}
            }
        }
    }
    
    var isSaved : Bool {
        guard let cards = cards else {
            return true
        }
        return !(manager.profile?.cards == nil || cards != manager.profile!.cards!)
    }
    
    @IBAction func addAction(sender: UIButton) {
        showPopover(sender)
    }
	
	private func openPurchase()
	{
		performSegueWithIdentifier(Segues.Purchase.rawValue, sender: self)
	}

    
    @IBAction func backAction(sender: AnyObject) {
		
		//TODO: return to Purchase if presed back action
		
//		if let _ = Storage.temp.popObjectForKey(kStorageBackToPurchase) as? Bool
//		{
//			openPurchase()
//		}
//		else
//		{
			Storage.temp.removeObjectForKey(kStorageBackToPurchase)
			performSegueWithIdentifier(Segues.Back.rawValue, sender: self)
//		}
    }
    
    func showPopover(base: UIView)
    {
        if let popover = editor.popoverPresentationController {
            popover.delegate = self
            popover.sourceView = base
            popover.sourceRect = base.bounds
            popover.permittedArrowDirections = [.Up, .Down]
            self.presentViewController(editor, animated: true, completion: nil)
        }
    }
    
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return .None
    }
}

extension ManageCardsViewController : CardEditorDelegate {
    func editor(editor: CardEditor, didEdit newCardViewModel: CardViewModel) {
        viewModel.append(newCardViewModel)
        if viewModel.count == 1 {
            tvCards.reloadData()
        } else {
            tvCards.insertRowsAtIndexPaths([NSIndexPath(forRow: viewModel.count - 1, inSection: 0)], withRowAnimation: .Automatic)
        }
        saveProfile()
        editor.reset()
        dismissViewControllerAnimated(true, completion: nil)
    }
    func editorDidCancel(editor: CardEditor) {
        dismissViewControllerAnimated(true, completion: nil)
    }
}

extension ManageCardsViewController : UITableViewDelegate, UITableViewDataSource {
    
    private func hasCards() -> Bool {
        return viewModel.count > 0
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return hasCards() ? viewModel.count : 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if hasCards() {
            let cell = tableView.dequeueReusableCellOfType(ProfilePayMethodCell.self)
            cell.configure(with: viewModel[indexPath.row])
            return cell
        } else {
            let cell = tableView.dequeueReusableCellOfType(CommonEmptyResultsCell.self)
            cell.configure(with: emptyViewModel)
            return cell
        }
    }
    
    func tableView(tableView: UITableView, shouldHighlightRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return indexPath.row > 0 || hasCards()
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if hasCards() {
            selectedCard = viewModel[indexPath.row].card
            performSegueWithIdentifier(Segues.PrimaryCard.rawValue, sender: self)
        }
    }
}
