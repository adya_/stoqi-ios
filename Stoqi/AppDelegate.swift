import UIKit
import IQKeyboardManagerSwift
import Foundation
import FBSDKCoreKit

private let kStorageShowIntro = "kStorageShowIntro"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    override init() {
        Injector.configure(with: DummyInjectionPreset().managersRules) // To add all managers
        Injector.configure(with: ProductionInjectionPreset())
      //  Injector.configure(with: DummyInjectionPreset().managersRules) // To override managers with dummy
        Injector.printConfiguration()
    }
    
    private func launch() {
        let show = Storage.local[kStorageShowIntro] as? Bool ?? true
        window = UIWindow(frame: UIScreen.mainScreen().bounds)
        if show {
            Storage.local[kStorageShowIntro] = false
            window?.rootViewController = UIStoryboard(name: "Intro", bundle: nil).instantiateInitialViewController()
        } else {
            window?.rootViewController = UIStoryboard(name: "Login", bundle: nil).instantiateInitialViewController()
        }
        window?.makeKeyAndVisible()
    }
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        IQKeyboardManager.sharedManager().enable = true
        TestFairy.begin("8b0874a39eda7d685f5c86bfeab27646d9d4fa94")
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        launch()
        return true
    }
    
    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool {
        return FBSDKApplicationDelegate.sharedInstance().application(application,
                                                                     openURL: url,
                                                                     sourceApplication: sourceApplication,
                                                                     annotation: annotation)
    }
    
    @available(iOS 9.0, *)
    func application(app: UIApplication, openURL url: NSURL, options: [String : AnyObject]) -> Bool {
        return FBSDKApplicationDelegate.sharedInstance().application(app,
                                                                     openURL: url,
                                                                     sourceApplication: options[UIApplicationOpenURLOptionsSourceApplicationKey] as? String,
                                                                     annotation: options [UIApplicationOpenURLOptionsAnnotationKey])
    }
    
    func applicationDidBecomeActive(application: UIApplication) {
        FBSDKAppEvents.activateApp()
    }
}

