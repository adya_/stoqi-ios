class ProfilePayMethodCell: UITableViewCell, TSTableViewElement {
    
    @IBOutlet weak private var ivImage: UIImageView!
    @IBOutlet weak private var lCard: UILabel!
    @IBOutlet weak private var ivValid: UIImageView!
    
    func configure(with dataSource: ProfilePayMethodCellDataSource) {
        
        if let card = dataSource.cardNumber {
            let cardLength : Int = card.characters.count
            let index = (cardLength > 4 ? card.endIndex.advancedBy(-4) : card.startIndex)
            let lastDigits =  card.substringFromIndex(index)
            self.lCard.text = "•••• \(lastDigits)"
            self.ivImage.image = dataSource.cardTypeImage
        } else {
            self.lCard.text = "Payment method not defined"
            self.ivImage.image = nil
        }
    }
}

// MARK: - Selection
extension ProfilePayMethodCell {
    override func setHighlighted(highlighted: Bool, animated: Bool) {
        UIView.animateWithDuration(animated ? 0.7 : 0, animations: {
            self.select(highlighted)
        })
        super.setHighlighted(highlighted, animated: animated)
    }
    
    private func select(selected : Bool){
        if selected {
            self.contentView.backgroundColor = StoqiPallete.mainLightColor
        }
        else {
            self.contentView.backgroundColor = UIColor.whiteColor()
        }
    }
}

protocol ProfilePayMethodCellDataSource {
    var cardTypeImage : UIImage? {get}
    var cardNumber : String? {get}
    var isValid : Bool {get}
}