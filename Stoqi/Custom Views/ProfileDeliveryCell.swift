class ProfileDeliveryCell: UITableViewCell, TSTableViewElement {
    
    @IBOutlet weak private var lAddress: UILabel!
    
    
    func configure(with dataSource: ProfileDeliveryCellDataSource) {
        self.lAddress.text = dataSource.fullAddress ?? "No address set"
    }
}

// MARK: - Selection
extension ProfileDeliveryCell {
    override func setHighlighted(highlighted: Bool, animated: Bool) {
        UIView.animateWithDuration(animated ? 0.7 : 0, animations: {
            self.select(highlighted)
        })
        super.setHighlighted(highlighted, animated: animated)
    }
    
    private func select(selected : Bool){
        if selected {
            self.contentView.backgroundColor = StoqiPallete.mainLightColor
        }
        else {
            self.contentView.backgroundColor = UIColor.whiteColor()
        }
    }
}

protocol ProfileDeliveryCellDataSource {
    var fullAddress : String? {get}
}
