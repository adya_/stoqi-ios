class RegisterListIndicatorView : UILabel, TSIndicatorView {
    
    var defaultColor : UIColor = UIColor.lightGrayColor()
    var activeColor : UIColor = UIColor.darkGrayColor()
    
    init(title : String, defaultColor : UIColor, activeColor : UIColor) {
        super.init(frame: CGRectZero)
        self.defaultColor = defaultColor
        self.activeColor = activeColor
        self.text = title
        self.backgroundColor = self.defaultColor
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.backgroundColor = self.defaultColor
    }
    
    
    func changeIndicatorState(active: Bool, animated : Bool) {
        let color = active ? self.activeColor : self.defaultColor
        if animated {
            UIView.animateWithDuration(0.4, animations: {
                self.backgroundColor = color
            })
        } else {
            self.backgroundColor = color
        }
        
    }
}