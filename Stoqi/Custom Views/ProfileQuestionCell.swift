import UIKit

class ProfileQuestionCell: UITableViewCell, TSTableViewElement {
    
    @IBOutlet weak private var lQuestion: UILabel!
    @IBOutlet weak private var lAnswer: UILabel!
    
    static let height : CGFloat = 80
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func configure(with dataSource: ProfileQuestionCellDataSource) {
        self.lQuestion.text = dataSource.question
        self.lAnswer.text = dataSource.answer
    }
}

// MARK: - Selection
extension ProfileQuestionCell {
    override func setHighlighted(highlighted: Bool, animated: Bool) {
        UIView.animateWithDuration(animated ? 0.7 : 0, animations: {
            self.select(highlighted)
        })
        super.setHighlighted(highlighted, animated: animated)
    }
    
    private func select(selected : Bool){
        if selected {
            self.lQuestion.textColor = UIColor.whiteColor()
            self.contentView.backgroundColor = StoqiPallete.mainLightColor
        }
        else {
            self.lQuestion.textColor = StoqiPallete.grayTextColor
            self.contentView.backgroundColor = UIColor.whiteColor()
        }
    }
}

protocol ProfileQuestionCellDataSource {
    var question : String {get}
    var answer : String? {get}
}
