class StockProductItemCell: UITableViewCell, TSTableViewElement {
    static let height : CGFloat = 80
    
    @IBOutlet weak private var lTitle: UILabel!
    @IBOutlet weak private var lBrand: UILabel!
    @IBOutlet weak private var lDetails: UILabel!
    @IBOutlet weak private var lUnits: UILabel!
    @IBOutlet weak private var pvStock: UIProgressView!
    
    func configure(with dataSource: StockProductItemCellDataSource) {
        self.lTitle.text = dataSource.item
        self.lBrand.text = dataSource.brand
        self.lDetails.text = dataSource.details
        let progress = dataSource.left / dataSource.total
        let stringProgress = String(format:"%.1f", progress)
        self.lUnits.text = "\(stringProgress) units"
        self.pvStock.progress = progress
    }
}

// MARK: - Selection
extension StockProductItemCell {
    override func setHighlighted(highlighted: Bool, animated: Bool) {
        UIView.animateWithDuration(animated ? 0.7 : 0, animations: {
            self.select(highlighted)
        })
        super.setHighlighted(highlighted, animated: animated)
    }
    
    private func select(selected : Bool){
        if selected {
            self.lTitle.textColor = UIColor.whiteColor()
            self.contentView.backgroundColor = StoqiPallete.lightColor
        }
        else {
            self.lTitle.textColor = StoqiPallete.accentColor
            self.contentView.backgroundColor = UIColor.whiteColor()
        }
    }
    
}


protocol StockProductItemCellDataSource {
    var item : String {get}
    var total : Float {get}
    var left : Float {get}
    var brand : String {get}
    var details : String {get}
}