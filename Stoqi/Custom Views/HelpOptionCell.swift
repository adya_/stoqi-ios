class HelpOptionCell: UITableViewCell, TSTableViewElement {
    
    @IBOutlet weak private var ivIcon: UIImageView!
    @IBOutlet weak private var lTitle: UILabel!
    @IBOutlet weak private var ivOpen: UIImageView!
    
    static var height : CGFloat {
        return 90
    }
    
    func configure(with dataSource: HelpOptionCellDataSource) {
        ivIcon.image = dataSource.icon
        lTitle.text = dataSource.title
        ivOpen.hidden = !dataSource.canOpen
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        if selected {
            if animated {
                UIView.transitionWithView(self,
                                          duration:0.5,
                                          options: UIViewAnimationOptions.TransitionCrossDissolve,
                                          animations: { self.setSelected() },
                                          completion: nil)
            } else {
                self.setSelected()
            }
            
        } else {
            self.selectedBackgroundView = nil
        }
    }
    
    private func setSelected() {
        self.selectedBackgroundView = UIImageView(image: UIImage(named:"misc_background"))
    }
    
}

protocol HelpOptionCellDataSource {
    var title : String {get}
    var icon : UIImage {get}
    var canOpen : Bool {get}
}
