class HistoryRequestCell : UITableViewCell, TSTableViewElement {
    
    @IBOutlet weak private var lDate: UILabel!
    @IBOutlet weak private var lInfo: UILabel!
    @IBOutlet weak private var vNextLine: UIView!
    
    static let height : CGFloat = 90
    
    func configure(with dataSource: HistoryRequestCellDataSource) {
        self.vNextLine.backgroundColor = dataSource.hasNextNode ? StoqiPallete.darkColor : StoqiPallete.grayColor
        self.lInfo.text = "\(dataSource.items) Items for R$ \(String(format:"%.2f",dataSource.price))"
        let date = dataSource.date.toString(withFormat: "dd MMM yyyy")
        self.lDate.text = "Delivered on \(date)"
        self.contentView.backgroundColor = StoqiPallete.lightColor
    }
}

class CurrentRequestCell : UITableViewCell, TSTableViewElement {
    
    @IBOutlet weak private var lDate: UILabel!
    @IBOutlet weak private var lInfo: UILabel!
    @IBOutlet weak private var lTitle: UILabel!
    
    static let height : CGFloat = 90
    
    func configure(with dataSource: CurrentRequestCellDataSource) {
        self.lInfo.text = "\(dataSource.items) items for delivery between"
		
		if let startDate = dataSource.startDate?.toString(withFormat: "dd MMM"),
			endDate = dataSource.endDate?.toString(withFormat: "dd MMM")
		{
			self.lDate.text = "\(startDate) - \(endDate)"
		}
		else
		{
			self.lDate.text = nil
		}
		
		
    }
}

// MARK: - Selection
extension CurrentRequestCell {
    override func setHighlighted(highlighted: Bool, animated: Bool) {
        UIView.animateWithDuration(animated ? 0.7 : 0, animations: {
            self.select(highlighted)
        })
        super.setHighlighted(highlighted, animated: animated)
    }
    
    private func select(selected : Bool){
        if selected {
            self.lDate.textColor = UIColor.whiteColor()
            self.lTitle.textColor = UIColor.whiteColor()
            self.contentView.backgroundColor = StoqiPallete.mainLightColor
        }
        else {
            self.lDate.textColor = StoqiPallete.mainColor
            self.lTitle.textColor = StoqiPallete.mainColor
            self.contentView.backgroundColor = UIColor.whiteColor()
        }
    }
}

extension HistoryRequestCell {
    override func setHighlighted(highlighted: Bool, animated: Bool) {
        UIView.animateWithDuration(animated ? 0.7 : 0, animations: {
            self.select(highlighted)
        })
        super.setHighlighted(highlighted, animated: animated)
    }
    
    private func select(selected : Bool){
        if selected {
            self.contentView.backgroundColor = StoqiPallete.mainLightColor
        }
        else {
            self.contentView.backgroundColor = StoqiPallete.lightColor
        }
    }
}


protocol HistoryRequestCellDataSource {
    var items : Int {get}
    var date : NSDate {get}
    var price : Float {get}
    var hasNextNode : Bool {get set}
}

protocol CurrentRequestCellDataSource {
    var items : Int {get}
    var startDate : NSDate? {get}
    var endDate : NSDate? {get}
}