class ProductItemCell: UITableViewCell, TSTableViewElement {
    static let height : CGFloat = 60
    
    @IBOutlet weak private var lQuantity: UILabel!
    @IBOutlet weak private var lTitle: UILabel!
    @IBOutlet weak private var lBrand: UILabel!
    @IBOutlet weak private var lDetails: UILabel!
    @IBOutlet weak private var ivEditable: UIImageView!
    
    var canEdit : Bool = true {
        didSet {
            self.ivEditable.hidden = !canEdit
        }
    }
    
    func configure(with dataSource: ProductItemCellDataSource) {
        self.lQuantity.text = "\(dataSource.quantity)"
        self.lTitle.text = dataSource.item
        self.lBrand.text = dataSource.brand
        self.lDetails.text = dataSource.details
    }
}

// MARK: - Selection
extension ProductItemCell {
    override func setHighlighted(highlighted: Bool, animated: Bool) {
        UIView.animateWithDuration(animated ? 0.7 : 0, animations: {
                self.select(highlighted)
            })
        super.setHighlighted(highlighted, animated: animated)
    }
    
    private func select(selected : Bool){
        if selected {
            self.lTitle.textColor = UIColor.whiteColor()
            self.contentView.backgroundColor = StoqiPallete.mainLightColor
        }
        else {
            self.lTitle.textColor = StoqiPallete.mainColor
            self.contentView.backgroundColor = UIColor.whiteColor()
        }
    }

}


protocol ProductItemCellDataSource {
    var item : String {get}
    var quantity : Int {get}
    var brand : String {get}
    var details : String {get}
}