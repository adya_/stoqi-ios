
enum StoqiTab : Int {
	case Main = 0
	case Requests
	case Analysis
	case School
	case Settings
}

class StoqiTabBarController: UITabBarController {
	
	func openTab(tab : StoqiTab) -> UIViewController?
	{
		selectedIndex = tab.rawValue
		return viewControllers?[selectedIndex]
	}
}
