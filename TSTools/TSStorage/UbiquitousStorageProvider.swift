class UbiquitousStorageProvider : StorageProvider {
    private var storage = NSUbiquitousKeyValueStore.defaultStore()
    
    subscript(key : String) -> Any? {
        set {
            if newValue == nil {
                storage.setObject(nil, forKey: key)
            } else if let newValue = newValue as? AnyObject {
                storage.setObject(newValue, forKey: key)
            } else {
                print("\(self.dynamicType): Value can't be stored.")
            }
        }
        get {
            return storage.objectForKey(key)
        }
    }
    
    func removeAll() {
        let keys = dictionary.keys
        for key in keys {
            storage.removeObjectForKey(key)
        }
        storage.synchronize()
    }
    
    
    var count: Int {
        get {
            return storage.dictionaryRepresentation.count
        }
    }
    
    var dictionary: [String : Any]{
        return storage.dictionaryRepresentation
    }
    
    deinit{
        storage.synchronize()
    }
}