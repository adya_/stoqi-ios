/**
 Highly configurable powerful injection mechanism.
 
 - Note: Best place to configure `Injector` is in the `AppDelegate`'s `init` method. (This will ensures that by the time you are injecting constants in ViewControllers init methods `Injector` will be ready for that).
 
 - Version:    1.1
 - Date:       11/22/2016
 - Since:      11/03/2016
 - Author:     AdYa
 */
class Injector {
   
    /// TSTOOLS: Upgrade Injector with ability to specify `for: Any.Type` parameter of injection to distinct injection of the same thing in different places.
    
    /// Used to tag log messages instead of `String(self.dynamicType)` to avoid extra `.Type` at the end of the resulting string.
    private static let TAG = "Injector"
    
    /// Internal property to store configured injection rules.
    private static var rules : [String : [String : InjectionRule]] = [:]
    
    private static var cache : [String : [String : Any]] = [:]
    
    /// Replaces existing `InjectionRule`s with specified in the preset.
    /// - Parameter preset: An array of rules to be set.
    static func configure(with preset : InjectionRulesPreset) {
        self.configure(with: preset.rules)
    }
    
    /// Replaces existing `InjectionRule`s with specified.
    /// - Parameter rules: An array of rules to be set.
    static func configure(with rules : [InjectionRule]) {
        rules.forEach { self.addInjectionRule($0) }
    }
    
    /// Adds a single `InjectionRule` to existing rules.
    /// - Parameter rule: A rule to be added.
    static func addInjectionRule(rule : InjectionRule) {
        let protocolType = "\(rule.protocolType)"
        let targetType = "\(rule.targetType)"
        // Either add to existing sub-dictionary
        if self.rules[protocolType] != nil {
            self.rules[protocolType]?[targetType] = rule
        } else {
            self.rules[protocolType] = [targetType : rule]
        }
    }
    
    /**
     Injects concrete type conformed to target injectable type.
     - Parameter injectable: Protocol to which injected instance conforms.
     - Parameter parameter: Custom parameter to be used during injection.
     - Parameter InjectableType: Type of the `injectable`.
     
     - Throws:
         * InjectionError.UndefinedInjectionError
         * InjectionError.ParameterCastingError
     */
    static func inject<InjectableType : Any> (injectable : InjectableType.Type, with parameter: Any? = nil) throws -> InjectableType {
        let target : Any.Type // infer target type from given parameter. By default injection rules applied to Any.
        if let param = parameter {
            target = param.dynamicType
        } else {
            target = Any.Type.self
        }
        let protocolType = "\(injectable)"
        let targetType = "\(target)"
        
        guard let rule = self.rules[protocolType]?[targetType] else {
            print("\(TAG): Didn't find any rule suitable for injection of '\(injectable)' for target '\(target)'.")
            throw InjectionError.UndefinedInjectionError
        }
        
        if rule.once, let cached = self.cache[protocolType]?[targetType] as? InjectableType {
            print("\(TAG): Restored cached '\(injectable.dynamicType)' with '\(cached.dynamicType)'.")
            return cached
        }
        
        guard let injected = try rule.injection(parameter) as? InjectableType else {
            print("\(TAG): '\(injectable.dynamicType)' injection failed.")
            throw InjectionError.UndefinedInjectionError
        }
        print("\(TAG): Successfully injected '\(injectable.dynamicType)' with '\(injected.dynamicType)'.")
        if rule.once {
            if self.cache[protocolType] != nil {
                self.cache[protocolType]?[targetType] = injected
            } else {
                self.cache[protocolType] = [targetType : injected]
            }
        }
        return injected
    }
    
    /// Prints all configured injection rules.
    static func printConfiguration() {
        print("\(TAG): Configured injection rules: \n")
        self.rules
            .flatMap{$0.1.values}
            .sort{"\($0.0.protocolType)".compare("\($0.1.protocolType)") == .OrderedAscending }
            .forEach { print("\($0)")}
    }
}

/// Represents error occured during injection.
enum InjectionError : ErrorType {
    
    /// Represents failed attempt to cast provided parameter to the type required in the injection closure.
    case ParameterCastingError
    
    /// Represents failed attempt to inject type for which `Injector` either hasn't got suitable `InjectionRule` or provided rule was invalid.
    case UndefinedInjectionError
}

/// Represents an injection rule. This defines how Injector should construct concrete object for specified protocol type.
struct InjectionRule : CustomStringConvertible {
    
    private typealias InjectionClosure = ((Any?) throws -> Any)
    
    /// Intenral holder for the protocol type.
    private let protocolType : Any.Type
    
    /// Internal holder for the specific target type of the injection.
    private let targetType : Any.Type
    
    /// Indicates whether the injection should reuse object created before or not.
    private let once : Bool
    
    /// Internal holder for an injection closure.
    private let injection : InjectionClosure
    
    var description : String {
        return "\(self.protocolType) [\(self.targetType)]"
    }
    
    private init(protocolType : Any.Type, targetType : Any.Type = Any.Type.self, once : Bool = false, injection : InjectionClosure) {
        self.protocolType = protocolType
        self.targetType = targetType
        self.once = once
        self.injection = injection
    }
    
    /// Initializes `InjectionRule` with specified target type and injection closure with paramter.
    /// - Parameter injectable: Target protocol type to which this rule will be applied.
    /// - Parameter once: Indicates whether the injection should reuse object created before or not.
    /// - Parameter injection: A closure to which type instantiation is delegated.
    /// - Parameter InjectableType: Type of injectable which must confrom to general Injectable protocol.
    /// - Parameter ParameterType: Type of the parameter which will be passed to the closure.
    init<InjectableType> (injectable : InjectableType.Type, once : Bool = false, injection : () throws -> InjectableType) {
        self.init(protocolType: injectable, once: once) { _ in try injection() }
    }
    
    /// Initializes `InjectionRule` with specified target type and injection closure with paramter.
    /// - Parameter injectable: Target protocol type to which this rule will be applied.
    /// - Parameter once: Indicates whether the injection should reuse object created before or not.
    /// - Parameter injection: A closure to which type instantiation is delegated.
    /// - Parameter InjectableType: Type of injectable which must confrom to general Injectable protocol.
    /// - Parameter ParameterType: Type of the parameter which will be passed to the closure.
    init<InjectableType, TargetType> (injectable : InjectableType.Type, targetType : TargetType.Type, once : Bool = false, injection : (TargetType) throws -> InjectableType) {
        self.init(protocolType: injectable, targetType: targetType, once: once) {
            guard let parameter = $0 else {
                print("\(Injector.TAG): Unexpected nil parameter while injecting '\(injectable.dynamicType)'. Expected '\(TargetType.Type.self)'.")
                throw InjectionError.ParameterCastingError
            }
            guard let castedParameter = parameter as? TargetType else {
                print("\(Injector.TAG): Failed to cast parameter for injection of '\(injectable.dynamicType)'. Expected '\(TargetType.Type.self)', but actual parameter is of type '\(parameter.dynamicType)'.")
                throw InjectionError.ParameterCastingError
            }
            return try injection(castedParameter)
        }
    }
    
    /// Initializes `InjectionRule` with specified target type and injection closure without parameters.
    /// - Attention: This type of injection will use a single instance to inject it wherever it's requested. (Applicable to classes, since value types will be copied during injection).
    /// - Parameter injectable: Target protocol type to which this rule will be applied.
    /// - Parameter injection: A closure to which type instantiation is delegated.
    /// - Parameter InjectableType: Type of injectable which must confrom to general Injectable protocol.
    init<InjectableType> (injectable : InjectableType.Type, @autoclosure(escaping) injected : () throws -> InjectableType) {
        self.init(protocolType: injectable, once: true) { _ in return try injected() }
    }
}

/// Convinient way to pass in injection rules into `Injector`.
protocol InjectionRulesPreset {
    
    /// Rules to be used by `Injector`.
    var rules : [InjectionRule] {get}
}

/// TSTOOLS: Can't be used to constraint injection parameters type because of the way generic protocol are implemented in Swift... (e.g. generic constraint where Injectable.InjectionParameters == ParametersType)

/// Represents any injectable type. Used as an additional precaution to protect you from unintended types to be passed to Injector.
//protocol Injectable {
//        /// Type of Injection parameters.
//        associatedtype InjectionParameters
//}