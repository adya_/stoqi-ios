import UIKit

@objc class TSButton: UIButton {
    typealias DidTapButton = (TSButton) -> ()
    
    var didTouchUpInside: DidTapButton? {
        didSet {
            if didTouchUpInside != nil {
                addTarget(self, action: #selector(didTouchUpInside(_:)), forControlEvents: .TouchUpInside)
            } else {
                removeTarget(self, action: #selector(didTouchUpInside(_:)), forControlEvents: .TouchUpInside)
            }
        }
    }
    
    // MARK: - Actions
    
    func didTouchUpInside(sender: UIButton) {
        if let handler = didTouchUpInside {
            handler(self)
        }
    }
}
