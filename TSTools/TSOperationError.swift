/** Errors which may occur during operations in Data Layer.
 
 - Version:    1.0
 - Since:      11/03/2016
 - Author:     AdYa
 */
enum OperationError : ErrorType {
    case NotAuthorized
    case InvalidResponse
    case NetworkError
    case InvalidParameters
    case Unknown
	
	init(error: RequestError)
	{
		switch error {
		case .ServerError, .NetworkError:
			self = .NetworkError
			
		case .InvalidRequest:
			self = .InvalidParameters
			
		case .InvalidResponseKind, .FailedRequest:
			self = .InvalidResponse
			
		case .AuthorizationError:
			self = .NotAuthorized
		}
	}
}

/** Represents a result of any operation in Data Layer without any additional data attached to `Success` result.
 
 - Version:    1.0
 - Since:      11/03/2016
 - Author:     AdYa
 */
enum AnyOperationResult {
    
    /// Result with no additional data.
    case Success
    
    /// Result with `OperationError` occured during the operation.
    case Failure(OperationError)
    
    init<T>(result : OperationResult<T>) {
        if case .Failure(let error) = result {
            self = .Failure(error)
        } else {
            self = .Success
        }
    }
}

/** Represents a result of any operation in Data Layer with additional data attached to `Success` result.
 
 - Parameter T: Type of the data to be attached to `Success` result.
 
 - Version:    1.0
 - Since:      11/03/2016
 - Author:     AdYa
 */
enum OperationResult<T> {
    
    /// Result with an additional data of type `T`.
    case Success(T)
    
    /// Result with an `OperationError` occured during the operation.
    case Failure(OperationError)
}

typealias OperationCallback = (AnyOperationResult) -> Void