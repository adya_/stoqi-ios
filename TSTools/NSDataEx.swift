// Date: 10/30/2016
enum DataSizeMetric : Int, CustomStringConvertible {
    case Bytes = 0
    case KBytes = 1
    case MBytes = 2
    case GBytes = 3
    case TBytes = 4
    
    var description: String {
        switch self {
        case .Bytes: return "B"
        case .KBytes: return "KB"
        case .MBytes: return "MB"
        case .GBytes: return "GB"
        case .TBytes: return "TB"
        }
    }
}

struct DataSize : CustomStringConvertible {
    let metric : DataSizeMetric
    let value : Double
    
    var description: String {
        return "\(String(format:"%.2f", self.value)) \(self.metric)"
    }
}

extension NSData {
    
    func dataSizeIn(metric : DataSizeMetric) -> DataSize {
        var size = Double(self.length)
        for _ in 0...metric.rawValue {
            size /= 1024
        }
        return DataSize(metric: metric, value: size)
    }
    
    /** Returns the largest DataSizeMetric which is greater than 1.
     
     data1.length // 1025 bytes.
     let size1 = data.dataSize // 1.001 (KBytes)
     
     data2.length // 1023 bytes
     let size2 = data.dataSize // 1023 (Bytes)
     */
    var dataSize : DataSize {
        var size = Double(self.length)
        var metricValue = 0
        for i in 0...DataSizeMetric.TBytes.rawValue {
            guard size > 1024 else {
                break
            }
            metricValue = i + 1
            size /= 1024
        }
        return DataSize(metric: DataSizeMetric(rawValue: metricValue)!, value: size)
    }
}