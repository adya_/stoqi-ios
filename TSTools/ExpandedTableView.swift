/// UITableView subclass which will resize itself to fit content without scrolling.
class ExpandedTableView: UITableView {
    
    override var contentSize : CGSize {
        didSet {
            self.invalidateIntrinsicContentSize()
        }
    }
    
    
    override func intrinsicContentSize() -> CGSize {
        self.layoutIfNeeded()
        return CGSizeMake(UIViewNoIntrinsicMetric, self.contentSize.height)
    }
    
}