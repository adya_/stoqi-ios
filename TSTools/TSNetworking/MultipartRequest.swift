/// Defines multipart request properties required to perform request call.
protocol MultipartRequest : Request {
    
    /// File representations with raw data.
    /// - Note: Default = nil.
    var files : [File<NSData>]? {get}
    
    /// File representations with URL paths.
    /// - Note: Default = nil.
    var filePaths : [File<NSURL>]? {get}
    
    /// Defines how parameters should be encoded when embedding into multi-part request.
    /// - Note: Default = NSUTF8StringEncoding.
    var parametersEncoding : UInt {get}
}

/// Represents a file to be uploaded
struct File<T> : CustomStringConvertible {
    let name : String
    let value : T
    let fileName : String
    let mimeType : String
    var description : String {
        return self.name
    }
}

// MARK: Defaults
extension MultipartRequest {
    var files : [File<NSData>]? {
        return nil
    }
    
    var filePaths : [File<NSURL>]? {
        return nil
    }
    
    var parametersEncoding : UInt {
        return NSUTF8StringEncoding
    }
    
    var encoding : RequestEncoding {
        return .FormData
    }
    
    var description: String {
        var descr = "\(self.method) '"
        if let baseUrl = self.baseUrl {
            descr += "\(baseUrl)/"
        }
        descr += "\(self.url)'"
        if let headers = self.headers {
            descr += "\nHeaders:\n\(headers)"
        }
        if let params = self.parameters {
            descr += "\nParameters:\n\(params)"
        }
        if let files = self.files {
            descr += "\nFiles:\n"
            files.forEach{
                descr += "\($0.name) of type '\($0.mimeType)' (size: \($0.value.dataSize))\n"
            }
        } else if let files = self.filePaths {
            descr += "\n Files:\n"
            files.forEach{
                descr += "\($0.name) of type '\($0.mimeType)' (at: \($0.value.absoluteString))\n"
            }
        }
        return descr
    }
}

extension File where T : NSData {
    var description: String {
        return "\(self.name). Type: \(self.mimeType). Size: \(self.value.dataSize)."
    }
}

extension File where T : NSURL {
    var description: String {
        return "\(self.name). Type: \(self.mimeType). Path: \(self.value.absoluteString)."
    }
    var fileName : String {
        return ""
    }
}