/// Represents result of the request.
enum ResponseResult <T: Any> {
    
    /// Response was successful and valid.
    /// - Parameter response: a response object.
    case Success(response : T)
    
    /// Request failed with an error.
    /// - Parameter error: Occured error.
    case Failure(error : RequestError)
}

enum Result {
    case Success
    case Failure(error : RequestError)
}

// MARK: - Conversion
extension Result {
    init(responseResult : AnyResponseResult) {
        switch responseResult {
        case .Success: self = .Success
        case .Failure(let error): self = .Failure(error: error)
        }
    }
}

typealias AnyResponseResult = ResponseResult<AnyResponse>