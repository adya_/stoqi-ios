/// Defines set of handles error.
enum RequestError : ErrorType {
    
    /// Error occured while remote service processing the request.
    case ServerError
    
    /// Request couldn't reach destination.
    case NetworkError
    
    /// Given `Request` object is not valid to be sent.
    case InvalidRequest
    
    /// Actual response has type different from specified `ResponseKind`. Therefore couldn't be handled by `Response`.
    case InvalidResponseKind
    
    /// Remote service correctly processed `Request`, but responsed with an error.
    case FailedRequest
    
    /// Remote service requires authorization, which either was not provided or was invalid.
    case AuthorizationError
}