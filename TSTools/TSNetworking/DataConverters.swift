/// TSTOOLS: 10/25/16.
class ResponseConverter <T> {
    func convert(dictionary : [String : AnyObject]) -> T? {return nil}
    
    func log(entity : AnyObject) {
        print("\(self.dynamicType): Failed to parse: \n \(entity)")
    }
}

class RequestConverter <T> {
    func convert(model : T) -> [String : AnyObject]? { return nil }
}